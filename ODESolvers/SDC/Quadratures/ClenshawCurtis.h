// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// Picard includes
#include "ODESolvers/SDC/Quadratures/Quadrature.hpp"

class ClenshawCurtis : public Quadrature<ClenshawCurtis>
{
public:
    enum QuadSize
    {
        Two       = 2,
        Three     = 3,
        Four      = 4,
        Five      = 5,
        Seven     = 7,
        Nine      = 9,
        Thirteen  = 13,
        Seventeen = 17
    };

public:
    template <QuadSize NumNodes>
    static MatrixType<NumNodes> matrix();

    template <QuadSize NumNodes>
    static NodesType<NumNodes> nodes();

    template <QuadSize NumNodes>
    static DeltasType<NumNodes> deltas();
};
