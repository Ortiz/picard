// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <cassert>
#include <memory>

// External dependencies
#include <Eigen/Core>

template <typename QuadratureType>
class Quadrature
{
public:
    template <int NumNodes>
    using MatrixType = Eigen::Map<Eigen::Matrix<double, NumNodes - 1, NumNodes, Eigen::RowMajor>>;

    template <int NumNodes>
    using NodesType = Eigen::Map<Eigen::Matrix<double, NumNodes, 1>>;

    template <int NumNodes>
    using DeltasType = Eigen::Map<Eigen::Matrix<double, NumNodes - 1, 1>>;

    template <int NumNodes>
    inline auto matrix() const && -> MatrixType<NumNodes>
    {
        return static_cast<QuadratureType *>(this)->template matrix<NumNodes>();
    }

    template <int NumNodes>
    inline auto nodes() const && -> NodesType<NumNodes>
    {
        return static_cast<QuadratureType *>(this)->template nodes<NumNodes>();
    }

    template <int NumNodes>
    inline auto deltas() const && -> DeltasType<NumNodes>
    {
        return static_cast<QuadratureType *>(this)->template deltas<NumNodes>();
    }
};
