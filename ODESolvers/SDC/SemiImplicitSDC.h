// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

// STL includes
#include <memory>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

class SemiImplicitSDC : public IterativeMethod<SemiImplicitSDC>
{
public:
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const double, const VectorType &)>;

    /**
     * @brief Create an explicit Euler method.
     * @param dt Fixed time step.
     * @param f Right hand side function.
     */
    SemiImplicitSDC(const double dt,
                    const FunctionType &fE,
                    const FunctionType &fI,
                    const size_t numCorrections = 1);

    SemiImplicitSDC() = delete;

    SemiImplicitSDC(const SemiImplicitSDC &other);

    SemiImplicitSDC(SemiImplicitSDC &&other);

    SemiImplicitSDC &operator=(const SemiImplicitSDC &other);

    SemiImplicitSDC &operator=(SemiImplicitSDC &&other);

    /**
     * @brief Explicit non-default definition is required.
     */
    ~SemiImplicitSDC();

    /**
     * @brief Set initial conditions.
     * @param x0 Initial vector, x0 = x(t0).
     * @param t0 Initial time.
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void step();

    /**
     * @brief Return last iterate
     * @return Latest iteration
     */
    const VectorType &get() const;

private:
    class SemiImplicitSDCTimeStepper;
    std::unique_ptr<SemiImplicitSDCTimeStepper> solver;
};
