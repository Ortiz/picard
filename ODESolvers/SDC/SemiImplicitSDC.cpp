// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "SemiImplicitSDC.h"

// External dependencies
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>

// Picard includes
#include "ODESolvers/Euler/BackwardEuler.h"
#include "Quadratures/ClenshawCurtis.h"
#include "Utilities/Timer.h"

class SemiImplicitSDC::SemiImplicitSDCTimeStepper
{
    using QuadSize = ClenshawCurtis::QuadSize;
    constexpr QuadSize getNumNodes() const noexcept { return QuadSize::Two; }
    using RightHandSideValuesType = Eigen::Matrix<double, Eigen::Dynamic, QuadSize::Two>;

public:
    SemiImplicitSDCTimeStepper(const double dt,
                               const FunctionType &fE,
                               const FunctionType &fI,
                               const size_t numCorrections);

    /**
     * @brief Initial conditions
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void iterate();

    /**
     * @brief Return last iteration
     */
    VectorType &solution();

private:
    /**
     * @brief Predictor. Use forward Euler to compute an initial guess.
     *        The main purpose of this method is to initialize this->f
     */
    void predictor();

    /**
     * @brief Correction method. Make correction on initial guess.
     */
    void corrector();

private:
    double timestep;          // Smallest time unit this solver should use to march forward
    FunctionType implicitRhs; // Implicit portion of Right-hand-side of the IVP
    FunctionType explicitRhs; // Implicit portion of Right-hand-side of the IVP
    double t;                 // Current time
    VectorType x;             // Current solution iterate x(t)

    RightHandSideValuesType implicitFunctionValues; // Implicit RHS values
    RightHandSideValuesType explicitFunctionValues; // Explicit RHS values
    size_t numCorrections;

    friend int TestSemiImplicitSDC(int, char **);
};

//-------------------------------------------------------------------------------------------------
// Definitions for SemiImplicitSDC::SemiImplicitSDCTimeStepper
//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::SemiImplicitSDCTimeStepper::SemiImplicitSDCTimeStepper(const double dt,
                                                                        const FunctionType &fE,
                                                                        const FunctionType &fI,
                                                                        const size_t numCorrections)
    : timestep(dt), explicitRhs(fE), implicitRhs(fI), numCorrections(numCorrections)
{
    this->t = 0.0;
}

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::SemiImplicitSDCTimeStepper::init(const double t0, const VectorType &x0)
{
    this->x = x0;
    this->t = t0;
    this->explicitFunctionValues.resize(x0.size(), QuadSize::Two);
    this->implicitFunctionValues.resize(x0.size(), QuadSize::Two);
    this->explicitFunctionValues.col(0) = this->explicitRhs(t0, x0);
    this->implicitFunctionValues.col(0) = this->implicitRhs(t0, x0);
}

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::SemiImplicitSDCTimeStepper::iterate()
{
    getLogger()->info("SemiImplicitSDC::iterate(): "
                      "Evaluating the explicit SDC method with size: {0}",
                      this->x.size());

    Timer timeThis("SemiImplicitSDC::iterate()");

    // Save initial state
    auto x0 = this->x;
    auto t0 = this->t;

    this->predictor();

    getLogger()->trace("Starting corrector loop.");
    for (size_t k = 0; k < this->numCorrections; ++k)
    {
        getLogger()->trace("Performing correction {}.", k);

        // Reset to the initial state before doing a correction.
        this->x = x0;
        this->t = t0;
        this->corrector();
    }

    // Update explicit and implicit rhs with the latest rhs function
    // Note: the size of delta is one less than the numbers of columns in
    // the rhs functions.
    this->explicitFunctionValues.col(0)
      = this->explicitFunctionValues.col(ClenshawCurtis::deltas<QuadSize::Two>().size());
    this->implicitFunctionValues.col(0)
      = this->implicitFunctionValues.col(ClenshawCurtis::deltas<QuadSize::Two>().size());
}

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::VectorType &SemiImplicitSDC::SemiImplicitSDCTimeStepper::solution() { return this->x; }

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::SemiImplicitSDCTimeStepper::predictor()
{
    Timer timeThis("SemiImplicitSDC::predictor()");
    ClenshawCurtis quadrature;
    auto deltas        = this->timestep * ClenshawCurtis::deltas<QuadSize::Two>();
    auto backwardEuler = [this](const double dt, const VectorType &rhs)
    {
        auto solver = BackwardEuler::New(dt, this->implicitRhs);
        solver->init(t, x + dt * rhs);
        solver->step();
        return solver->get();
    };

    for (size_t i = 0; i < deltas.size(); ++i)
    {
        this->t += deltas(i);
        this->x = backwardEuler(deltas(i), this->explicitFunctionValues.col(i));

        this->explicitFunctionValues.col(i + 1) = this->explicitRhs(t, x);
        this->implicitFunctionValues.col(i + 1) = this->implicitRhs(t, x);
    }
}

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::SemiImplicitSDCTimeStepper::corrector()
{
    Timer timeThis("SemiImplicitSDC::corrector()");

    // The quadrature is on the [0,1] interval so we need to scale deltas and quadrature
    auto quadCoefficientMatrix = this->timestep * ClenshawCurtis::matrix<QuadSize::Two>();
    auto deltas                = this->timestep * ClenshawCurtis::deltas<QuadSize::Two>();

    // Initialize the rhs difference vector to zero.
    VectorType diff    = VectorType::Zero(this->x.size());
    auto backwardEuler = [this](const double dt, const VectorType &rhs)
    {
        auto solver = BackwardEuler::New(dt, this->implicitRhs);
        solver->init(t, x + dt * rhs);
        solver->step();
        return solver->get();
    };

    auto integralMatrix
      = ((this->explicitFunctionValues + this->implicitFunctionValues) * quadCoefficientMatrix.transpose())
          .eval();

    for (size_t i = 0; i < deltas.size(); ++i)
    {
        this->t += deltas(i);
        this->x
          = backwardEuler(deltas(i), diff - this->implicitFunctionValues.col(i + 1)) + integralMatrix.col(i);

        this->implicitFunctionValues.col(i + 1) = this->implicitRhs(t, x);
        diff = this->explicitRhs(t, x) - this->explicitFunctionValues.col(i + 1);
        this->explicitFunctionValues.col(i + 1) += diff;
    }
}

//-------------------------------------------------------------------------------------------------
// Definitions for SemiImplicitSDC
//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::SemiImplicitSDC(const double dt,
                                 const FunctionType &fE,
                                 const FunctionType &fI,
                                 const size_t numCorrections)
    : solver(new SemiImplicitSDCTimeStepper(dt, fE, fI, numCorrections))
{
}

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::SemiImplicitSDC(const SemiImplicitSDC &other)
    : solver(new SemiImplicitSDCTimeStepper(*other.solver))
{
}

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::SemiImplicitSDC(SemiImplicitSDC &&other) : solver(std::move(other.solver)) { }

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC &SemiImplicitSDC::operator=(const SemiImplicitSDC &other)
{
    this->solver.reset(new SemiImplicitSDCTimeStepper(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC &SemiImplicitSDC::operator=(SemiImplicitSDC &&other)
{
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

SemiImplicitSDC::~SemiImplicitSDC() { }

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::init(const double t0, const VectorType &x0) { this->solver->init(t0, x0); }

//-------------------------------------------------------------------------------------------------

void SemiImplicitSDC::step() { this->solver->iterate(); }

//-------------------------------------------------------------------------------------------------

const SemiImplicitSDC::VectorType &SemiImplicitSDC::get() const { return this->solver->solution(); }
