// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

// STL includes
#include <memory>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

class ExplicitSDC : public IterativeMethod<ExplicitSDC>
{
public:
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const double, const VectorType &)>;

    /**
     * @brief Create an explicit Euler method.
     * @param dt Fixed time step.
     * @param f Right hand side function.
     */
    ExplicitSDC(const double dt, const FunctionType &f, const size_t numCorrections = 50);

    ExplicitSDC() = delete;

    ExplicitSDC(const ExplicitSDC &other);

    ExplicitSDC(ExplicitSDC &&other);

    ExplicitSDC &operator=(const ExplicitSDC &other);

    ExplicitSDC &operator=(ExplicitSDC &&other);

    /**
     * @brief Explicit non-default definition is required.
     */
    ~ExplicitSDC();

    /**
     * @brief Set initial conditions.
     * @param x0 Initial vector, x0 = x(t0).
     * @param t0 Initial time.
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Set initial conditions.
     * @param dt Timestep.
     * @param x0 Initial vector, x0 = x(t0).
     * @param t0 Initial time.
     */
    void init(const double dt, const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void step();

    /**
     * @brief Return last iterate
     * @return Latest iteration
     */
    const VectorType &get() const;

private:
    class ExplicitSDCTimeStepper;
    std::unique_ptr<ExplicitSDCTimeStepper> solver;
};
