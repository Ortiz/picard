// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "ExplicitSDC.h"

// Picard includes
#include "ODESolvers/Euler/ForwardEuler.h"
#include "Quadratures/ClenshawCurtis.h"
#include "Utilities/Timer.h"

class ExplicitSDC::ExplicitSDCTimeStepper
{
    using QuadSize                = ClenshawCurtis::QuadSize;
    using RightHandSideValuesType = Eigen::Matrix<double, Eigen::Dynamic, QuadSize::Two>;

public:
    ExplicitSDCTimeStepper(const double dt, const FunctionType &f, const size_t numCorrections);

    /**
     * @brief Initial conditions
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Initial conditions
     * @param dt Timestep
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     */
    void init(const double dt, const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void iterate();

    /**
     * @brief Return last iteration
     */
    VectorType &solution();

private:
    /**
     * @brief Predictor. This is equivalent to a ForwardEuler using a dt that varies.
     *        Uses forward Euler to compute an initial guess.
     *        The main purpose of this method is to initialize this->f
     */
    void predictor();

    /**
     * @brief Correction method. Make correction on initial guess.
     */
    void corrector();

private:
    double timestep;  // Smallest time unit this solver should use to march forward
    FunctionType rhs; // Right-hand-side of the IVP
    double t;         // Current time
    VectorType x;     // Current solution iterate x(t)

    RightHandSideValuesType f;
    size_t numCorrections;
};

//-------------------------------------------------------------------------------------------------
// Definitions for ExplicitSDC::ExplicitSDCTimeStepper
//-------------------------------------------------------------------------------------------------

ExplicitSDC::ExplicitSDCTimeStepper::ExplicitSDCTimeStepper(const double dt,
                                                            const FunctionType &f,
                                                            const size_t numCorrections)
    : timestep(dt), rhs(f), numCorrections(numCorrections)
{
    this->t = 0.0;
}

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::ExplicitSDCTimeStepper::init(const double t0, const VectorType &x0)
{
    this->x = x0;
    this->t = t0;
    this->f.resize(x0.size(), QuadSize::Two);
    this->f.col(0) = this->rhs(this->t, this->x);
}

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::ExplicitSDCTimeStepper::init(const double dt, const double t0, const VectorType &x0)
{
    this->timestep = dt;
    this->x        = x0;
    this->t        = t0;
    this->f.resize(x0.size(), QuadSize::Two);
    this->f.col(0) = this->rhs(this->t, this->x);
}

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::ExplicitSDCTimeStepper::iterate()
{
    getLogger()->trace("ExplicitSDC::iterate(): "
                       "Evaluating the explicit SDC method with size: {0}",
                       this->x.size());

    Timer timeThis("ExplicitSDC::iterate()");

    // Save initial state
    auto x0 = this->x;
    auto t0 = this->t;

    this->predictor();

    getLogger()->trace("Starting corrector loop.");
    for (size_t k = 0; k < this->numCorrections; ++k)
    {
        getLogger()->trace("Performing correction {}.", k);
        // Reset to the initial state before doing a correction.
        this->x = x0;
        this->t = t0;
        this->corrector();
    }

    // Set the first rhs vector to the latest rhs evaluation
    // Note: the size of delta is one less than the numbers of columns in
    // the rhs functions.
    this->f.col(0) = this->f.col(ClenshawCurtis::deltas<QuadSize::Two>().size());
}

//-------------------------------------------------------------------------------------------------

ExplicitSDC::VectorType &ExplicitSDC::ExplicitSDCTimeStepper::solution() { return this->x; }

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::ExplicitSDCTimeStepper::predictor()
{
    getLogger()->trace("Starting predictor loop.");
    Timer timeThis("ExplicitSDC::predictor()");

    auto deltas = this->timestep * ClenshawCurtis::deltas<QuadSize::Two>();

    for (size_t i = 0; i < deltas.size(); ++i)
    {
        this->x += deltas(i) * this->f.col(i);
        this->t += deltas(i);
        this->f.col(i + 1) = this->rhs(t, x);
    }
}

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::ExplicitSDCTimeStepper::corrector()
{
    Timer timeThis("ExplicitSDC::corrector()");

    // The quadrature is on the [0,1] interval so we need to scale deltas and quadrature
    auto quadCoefficientMatrix = this->timestep * ClenshawCurtis::matrix<QuadSize::Two>();
    auto deltas                = this->timestep * ClenshawCurtis::deltas<QuadSize::Two>();

    // Initialize the rhs difference vector to zero.
    VectorType diff = VectorType::Zero(this->x.size());

    // Use the quadrature to compute the integral of the right hand side function
    // Note: We do not want to do lazy evaluation here because the values of f are changing inside
    // the loop.
    auto integralMatrix = (this->f * quadCoefficientMatrix.transpose()).eval();
    for (size_t i = 0; i < deltas.size(); ++i)
    {
        this->t += deltas(i);
        this->x += deltas(i) * diff + integralMatrix.col(i);
        diff = this->rhs(t, x) - this->f.col(i + 1);
        this->f.col(i + 1) += diff;
    }
}

//-------------------------------------------------------------------------------------------------
// Definitions for ExplicitSDC
//-------------------------------------------------------------------------------------------------

ExplicitSDC::ExplicitSDC(const double dt, const FunctionType &f, const size_t numCorrections)
    : solver(new ExplicitSDCTimeStepper(dt, f, numCorrections))
{
}

//-------------------------------------------------------------------------------------------------

ExplicitSDC::ExplicitSDC(const ExplicitSDC &other) : solver(new ExplicitSDCTimeStepper(*other.solver)) { }

//-------------------------------------------------------------------------------------------------

ExplicitSDC::ExplicitSDC(ExplicitSDC &&other) : solver(std::move(other.solver)) { }

//-------------------------------------------------------------------------------------------------

ExplicitSDC &ExplicitSDC::operator=(const ExplicitSDC &other)
{
    this->solver.reset(new ExplicitSDCTimeStepper(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

ExplicitSDC &ExplicitSDC::operator=(ExplicitSDC &&other)
{
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

ExplicitSDC::~ExplicitSDC() { }

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::init(const double t0, const VectorType &x0) { this->solver->init(t0, x0); }

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::init(const double dt, const double t0, const VectorType &x0)
{
    this->solver->init(dt, t0, x0);
}

//-------------------------------------------------------------------------------------------------

void ExplicitSDC::step() { this->solver->iterate(); }

//-------------------------------------------------------------------------------------------------

const ExplicitSDC::VectorType &ExplicitSDC::get() const { return this->solver->solution(); }
