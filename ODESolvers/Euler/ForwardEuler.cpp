// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "ForwardEuler.h"

// Picard includes
#include "Utilities/Timer.h"

class ForwardEuler::ForwardEulerTimeStepper
{
public:
    ForwardEulerTimeStepper(const double dt, const FunctionType &f);

    /**
     * @brief Initial conditions
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     * @return (void)
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Initial conditions
     * @param dt Timestep
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     */
    void init(const double dt, const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void iterate();

    /**
     * @brief Return last iteration
     */
    VectorType &solution();

private:
    double timestep;  // Smallest time unit this solver should use to march forward
    FunctionType rhs; // Right-hand-side of the IVP
    double t;         // Current time
    VectorType x;     // Current solution iterate x(t)
};

//-------------------------------------------------------------------------------------------------
// Implementation for ForwardEuler::ForwardEulerTimeStepper
//-------------------------------------------------------------------------------------------------

ForwardEuler::ForwardEulerTimeStepper::ForwardEulerTimeStepper(const double dt, const FunctionType &f)
    : timestep(dt), rhs(f)
{
    getLogger()->trace("Instantiating the Forward Euler time stepping method.");
    this->t = 0.0;
}

//-------------------------------------------------------------------------------------------------

void ForwardEuler::ForwardEulerTimeStepper::init(const double t0, const VectorType &x0)
{
    getLogger()->trace("init() - Initial conditions, t0 = {0}, x0.size() = {1}", t0, x0.size());
    this->x = x0;
    this->t = t0;
}

//-------------------------------------------------------------------------------------------------

void ForwardEuler::ForwardEulerTimeStepper::init(const double dt, const double t0, const VectorType &x0)
{
    getLogger()->trace("init() - Initial conditions, dt= {0}, t0 = {1}, x0.size() = {2}", dt, t0, x0.size());
    this->timestep = dt;
    this->x        = x0;
    this->t        = t0;
}

//-------------------------------------------------------------------------------------------------

void ForwardEuler::ForwardEulerTimeStepper::iterate()
{
    getLogger()->trace("iterate() - Performing one step for t = {0}.", this->t);
    Timer timeThis("ForwardEuler::iterate()");
    this->x += this->timestep * this->rhs(this->t, this->x);
    this->t += this->timestep;
}

//-------------------------------------------------------------------------------------------------

ForwardEuler::VectorType &ForwardEuler::ForwardEulerTimeStepper::solution() { return this->x; }

//-------------------------------------------------------------------------------------------------
// Definitions for ForwardEuler
//-------------------------------------------------------------------------------------------------

ForwardEuler::ForwardEuler(const double dt, const FunctionType &f)
    : solver(new ForwardEulerTimeStepper(dt, f))
{
}

//-------------------------------------------------------------------------------------------------

ForwardEuler::ForwardEuler(const ForwardEuler &other) : solver(new ForwardEulerTimeStepper(*other.solver)) { }

//-------------------------------------------------------------------------------------------------

ForwardEuler::ForwardEuler(ForwardEuler &&other) : solver(std::move(other.solver)) { }

//-------------------------------------------------------------------------------------------------

ForwardEuler &ForwardEuler::operator=(const ForwardEuler &other)
{
    this->solver.reset(new ForwardEulerTimeStepper(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

ForwardEuler &ForwardEuler::operator=(ForwardEuler &&other)
{
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

ForwardEuler::~ForwardEuler() { }

//-------------------------------------------------------------------------------------------------

void ForwardEuler::init(const double t0, const VectorType &x0) { this->solver->init(t0, x0); }

//-------------------------------------------------------------------------------------------------

void ForwardEuler::init(const double dt, const double t0, const VectorType &x0)
{
    this->solver->init(dt, t0, x0);
}

//-------------------------------------------------------------------------------------------------

void ForwardEuler::step() { this->solver->iterate(); }

//-------------------------------------------------------------------------------------------------

const ForwardEuler::VectorType &ForwardEuler::get() const { return this->solver->solution(); }
