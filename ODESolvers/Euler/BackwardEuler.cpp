// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "BackwardEuler.h"

// Picard includes
#include "NonLinearSolvers/NewtonMethod.h"
#include "Utilities/Timer.h"

class BackwardEuler::BackwardEulerTimeStepper
{
public:
    BackwardEulerTimeStepper(const double dt, const FunctionType &f);

    /**
     * @brief Initial conditions
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     * @return (void)
     */
    void init(const VectorType &x0, const double t0);

    /**
     * @brief Initial conditions
     * @param dt Timestep
     * @param x0 x0 = x(t0)
     * @param t0 Initial time.
     */
    void init(const double dt, const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void iterate();

    /**
     * @brief Return last iteration
     */
    VectorType &solution();

    friend int TestBackwardEuler(int, char **);

private:
    double timestep;                      // Smallest time unit this solver should use to march forward
    FunctionType rhs;                     // Right-hand-side of the IVP
    std::shared_ptr<NewtonMethod> solver; // Non-linear solver
    double t;                             // Current time
    VectorType x;                         // Current solution iterate x(t)
};

//-------------------------------------------------------------------------------------------------
// Definitions for BackwardEuler::BackwardEulerTimeStepper
//-------------------------------------------------------------------------------------------------

BackwardEuler::BackwardEulerTimeStepper::BackwardEulerTimeStepper(const double dt, const FunctionType &f)
    : timestep(dt), rhs(f)
{
    getLogger()->trace("Instantiating the Backward Euler time stepping method.");
    solver = NewtonMethod::New(
      [this](const VectorType &y)
      {
          VectorType out = y - x - timestep * rhs(t, y);
          return out;
      });
}

//-------------------------------------------------------------------------------------------------

void BackwardEuler::BackwardEulerTimeStepper::init(const VectorType &x0, const double t0)
{
    getLogger()->trace("init() - Initial conditions, t0 = {0}, x0.size() = {1}", t0, x0.size());
    this->x = x0;
    this->t = t0;
}

//-------------------------------------------------------------------------------------------------

void BackwardEuler::BackwardEulerTimeStepper::init(const double dt, const double t0, const VectorType &x0)
{
    getLogger()->trace("init() - Initial conditions, dt= {0}, t0 = {1}, x0.size() = {2}", dt, t0, x0.size());
    this->timestep = dt;
    this->x        = x0;
    this->t        = t0;
}

//-------------------------------------------------------------------------------------------------

void BackwardEuler::BackwardEulerTimeStepper::iterate()
{
    getLogger()->trace("iterate() - Performing one step.");
    Timer timeThis("BackwardEuler::iterate()");
    this->solver->init(this->x);
    this->solver->solve();
    this->x = this->solver->get();
    this->t += this->timestep;
}

//-------------------------------------------------------------------------------------------------

BackwardEuler::VectorType &BackwardEuler::BackwardEulerTimeStepper::solution() { return this->x; }

//-------------------------------------------------------------------------------------------------
// Definitions for BackwardEuler
//-------------------------------------------------------------------------------------------------

BackwardEuler::BackwardEuler(const double dt, const FunctionType &f)
    : solver(new BackwardEulerTimeStepper(dt, f))
{
}

//-------------------------------------------------------------------------------------------------

BackwardEuler::BackwardEuler(const BackwardEuler &other) : solver(new BackwardEulerTimeStepper(*other.solver))
{
}

//-------------------------------------------------------------------------------------------------

BackwardEuler::BackwardEuler(BackwardEuler &&other) : solver(std::move(other.solver)) { }

//-------------------------------------------------------------------------------------------------

BackwardEuler &BackwardEuler::operator=(const BackwardEuler &other)
{
    this->solver.reset(new BackwardEulerTimeStepper(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

BackwardEuler &BackwardEuler::operator=(BackwardEuler &&other)
{
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

BackwardEuler::~BackwardEuler() { }

//-------------------------------------------------------------------------------------------------

void BackwardEuler::init(const double t0, const VectorType &x0) { this->solver->init(x0, t0); }

//-------------------------------------------------------------------------------------------------

void BackwardEuler::init(const double dt, const double t0, const VectorType &x0)
{
    this->solver->init(dt, t0, x0);
}

//-------------------------------------------------------------------------------------------------

void BackwardEuler::step() { this->solver->iterate(); }

//-------------------------------------------------------------------------------------------------

const BackwardEuler::VectorType &BackwardEuler::get() const { return this->solver->solution(); }
