// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <memory>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

/**
 * @brief Implementation of the explicit Euler time stepping method.
 */
class ForwardEuler : public IterativeMethod<ForwardEuler>
{
public:
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const double, const VectorType &)>;

public:
    /**
     * @brief Create an explicit Euler method.
     * @param dt Fixed time step.
     * @param f Right hand side function.
     */
    ForwardEuler(const double dt, const FunctionType &f);

    ForwardEuler() = delete;

    ForwardEuler(const ForwardEuler &other);

    ForwardEuler(ForwardEuler &&other);

    ForwardEuler &operator=(const ForwardEuler &other);

    ForwardEuler &operator=(ForwardEuler &&other);

    /**
     * @brief Explicit non-default definition is required.
     */
    ~ForwardEuler();

    /**
     * @brief Set initial conditions.
     * @param x0 Initial vector, x0 = x(t0).
     * @param t0 Initial time.
     */
    void init(const double t0, const VectorType &x0);

    /**
     * @brief Set initial conditions.
     * @param dt Timestep.
     * @param x0 Initial vector, x0 = x(t0).
     * @param t0 Initial time.
     */
    void init(const double dt, const double t0, const VectorType &x0);

    /**
     * @brief Complete one iteration.
     */
    void step();

    /**
     * @brief Return last iterate
     * @return Latest iteration
     */
    const VectorType &get() const;

private:
    /**
     * @brief Internal implementation of the Euler explicit time stepping method.
     */
    class ForwardEulerTimeStepper;
    std::unique_ptr<ForwardEulerTimeStepper> solver;
};
