// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "Logger.h"

// External dependencies
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

//-------------------------------------------------------------------------------------------------

Logger::Logger() { this->init(); }

//-------------------------------------------------------------------------------------------------

Logger::~Logger() { }

//-------------------------------------------------------------------------------------------------

Logger::AsyncPtr Logger::get(const std::string &name)
{
    if (!this->initialized)
    {
        this->init();
    }

    auto logger = std::dynamic_pointer_cast<spdlog::async_logger>(spdlog::get(name));

    if (nullptr != logger)
    {
        return logger;
    }

    std::vector<spdlog::sink_ptr> sinkList;
    for (const auto &s : this->sinks)
    {
        sinkList.push_back(s.second);
    }

    logger = std::make_shared<spdlog::async_logger>(
      name, sinkList.begin(), sinkList.end(), spdlog::thread_pool(), spdlog::async_overflow_policy::block);

    spdlog::register_logger(logger);
    return logger;
}

//-------------------------------------------------------------------------------------------------

void Logger::init()
{
    if (!this->initialized)
    {
        spdlog::init_thread_pool(8192, 2);
        this->sinks["console"] = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        this->sinks["console"]->set_level(spdlog::level::off);

        this->sinks["file"] = std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/picard.log", true);
        this->sinks["file"]->set_level(spdlog::level::info);
        this->initialized = true;
    }
}

//-------------------------------------------------------------------------------------------------

std::shared_ptr<Logger> Logger::instance()
{
    static auto logger = std::shared_ptr<Logger>(new Logger());
    return logger;
}

//-------------------------------------------------------------------------------------------------

void Logger::setLevel(const spdlog::level::level_enum level, const std::string &sinkType)
{
    if (!this->initialized)
    {
        this->init();
    }

    if (this->sinks.find(sinkType) != this->sinks.end())
    {
        this->sinks[sinkType]->set_level(level);
    }
    else
    {
        this->get("Logger")->warn("Logger::setLevel(): Unknown sink type.");
    }
}
