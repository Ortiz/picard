// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STD includes
#include <unordered_map>

// External dependencies
#include <spdlog/async.h>
#include <spdlog/spdlog.h>

/**
 * @brief Looger wrapper singleton. Wraps the spdlogger and manages creation of new loggers.
 */
class Logger
{
public:
    using AsyncPtr = std::shared_ptr<spdlog::async_logger>;

public:
    AsyncPtr get(const std::string &name);

    /**
     * @brief Get the singleton instance
     * @return Shared pointer to object of this class
     */
    static std::shared_ptr<Logger> instance();
    ~Logger();

    /**
     * @brief There are currently two sink types: console and file. The console log will write to the shell
     * and file sink will write to logs/picard.log. The default log level for the console sink is "warn" and
     * "trace" for the file sink. You can use this function to change those defaults.
     *
     * @param level The spdlog level, see spdlog::level::level_enum for details.
     * @param sinkType The label for the sink (stored in a map).  Possible options are "console" or "file"
     * @return (void)
     */
    void setLevel(const spdlog::level::level_enum level, const std::string &sinkType = "console");

private:
    Logger();
    Logger(const Logger &)  = delete;
    Logger(const Logger &&) = delete;

    Logger &operator=(const Logger &) = delete;
    Logger &operator=(const Logger &&) = delete;

    void init();

private:
    bool initialized = false;
    std::unordered_map<std::string, spdlog::sink_ptr> sinks;
};
