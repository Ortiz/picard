add_library(Utilities Timer.cpp Logger.cpp)
target_link_libraries(Utilities PRIVATE spdlog::spdlog)

target_include_directories(
  Utilities
  PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
          $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  INTERFACE $<INSTALL_INTERFACE:include/Picard>
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
            $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
)

generate_export_header(
  Utilities
  EXPORT_FILE_NAME
  ${CMAKE_CURRENT_BINARY_DIR}/UtilitiesExports.h
  EXPORT_MACRO_NAME
  UTILITIES_API
  PREFIX_NAME
  PICARD_
)

# Export symbols building.
set_target_properties(Utilities PROPERTIES DEFINE_SYMBOL "Utilities_EXPORTS")

install(TARGETS Utilities EXPORT PicardTargets)

install(
  DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  DESTINATION "include/Picard"
  FILES_MATCHING
  PATTERN "*.h"
)

install(
  DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  DESTINATION "include/Picard"
  FILES_MATCHING
  PATTERN CMakeFiles EXCLUDE
  PATTERN "*.h"
)
