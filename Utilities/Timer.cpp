// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "Timer.h"
#include "Logger.h"

//-------------------------------------------------------------------------------------------------

Timer::Timer(const std::string &scopeName)
    : startTimePoint(std::chrono::high_resolution_clock::now()), scopeName(scopeName),
      timeLogger(Logger::instance()->get("Timer"))
{
}

//-------------------------------------------------------------------------------------------------

int64_t Timer::end() const
{
    auto endTimePoint = std::chrono::high_resolution_clock::now();
    auto start        = std::chrono::time_point_cast<std::chrono::microseconds>(this->startTimePoint)
                   .time_since_epoch()
                   .count();
    auto end
      = std::chrono::time_point_cast<std::chrono::microseconds>(endTimePoint).time_since_epoch().count();

    return end - start;
}

//-------------------------------------------------------------------------------------------------

float Timer::get() const { return this->end() * 0.001f; }

//-------------------------------------------------------------------------------------------------

Timer::~Timer()
{
    // Log duration in milliseconds
    this->timeLogger->info(this->scopeName + ": took {0}ms", this->get());
}
