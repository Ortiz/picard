// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <chrono>
#include <map>
#include <string>

// Picard includes
#include "Core/Object.hpp"

/**
 * @brief Scope timer
 */
class Timer : public Object<Timer>
{
public:
    explicit Timer(const std::string &scopeName);
    ~Timer();

    /**
     * @brief Return the total time passed since the creation of the time object until now
     * @return time passed 64bit integer
     */
    int64_t end() const;

    /**
     * @brief Return the total time passed since the creation of the time object until now
     * @return time passed in microseconds
     */
    float get() const;

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> startTimePoint;
    std::string scopeName;
    Logger::AsyncPtr timeLogger;
};
