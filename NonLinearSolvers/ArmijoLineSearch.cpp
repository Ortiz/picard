// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "ArmijoLineSearch.h"

ArmijoLineSearch::ArmijoLineSearch(const FunctionType &f) : objectiveFunction(f), sigma {0.1, 0.5} { }

//-------------------------------------------------------------------------------------------------

double
ArmijoLineSearch::operator()(const VectorType &x, const VectorType &dx, std::array<double, 2> &functionNorms)
{
    // Vector of steplengths
    double lambda = 1.0;

    // Steplengths used in the parabolic model
    std::array<double, 2> stepLengths = {lambda, 1.0};

    // Exit early if unbounded
    functionNorms[1] = this->objectiveFunction(x - lambda * dx).norm();
    if (functionNorms[1] > std::numeric_limits<double>::max() || std::isinf(functionNorms[1])
        || std::isnan(functionNorms[1]))
    {
        getLogger()->critical("ArmijoLineSearch(): Unbounded function norm");
        this->convergenceStatus = Status::Diverged;
        return 1.0;
    }

    std::array<double, 3> squaredFunctionNorms = {functionNorms[0] * functionNorms[0]};
    squaredFunctionNorms[1] = functionNorms[1] * functionNorms[1]; // cppcheck-suppress containerOutOfBounds
    squaredFunctionNorms[2] = squaredFunctionNorms[1];             // cppcheck-suppress containerOutOfBounds

    for (size_t i = 0; i < this->armijoMaxIterations; ++i)
    {
        // Armijo condition
        if (functionNorms[1] < (1.0 - this->alpha * lambda) * functionNorms[0])
        {
            getLogger()->info("ArmijoLineSearch(): Armijo line search succeeded with stepLength = {0}",
                              lambda);
            this->convergenceStatus = Status::Converged;
            return lambda;
        }

        //
        lambda = (i == 0) ? this->sigma[1] : this->parabolicSearch(stepLengths, squaredFunctionNorms);
        getLogger()->info("ArmijoLineSearch(): Computed new reduction step, lambda = {0}", lambda);

        if (lambda < std::numeric_limits<double>::epsilon())
        {
            getLogger()->warn("ArmijoLineSearch(): Reached precission limits with lambda = {0}", lambda);
            this->convergenceStatus = Status::Unknown;
            return 1.0;
        }

        // keeps books on function norms and steplengths
        functionNorms[1] = this->objectiveFunction(x - lambda * dx).norm();
        if (functionNorms[1] > std::numeric_limits<double>::max() || std::isinf(functionNorms[1])
            || std::isnan(functionNorms[1]))
        {
            getLogger()->critical("ArmijoLineSearch(): Unbounded function norm");
            this->convergenceStatus = Status::Diverged;
            return 1.0;
        }

        squaredFunctionNorms[1] = squaredFunctionNorms[2];
        squaredFunctionNorms[2] = functionNorms[1] * functionNorms[1];
        stepLengths[0]          = stepLengths[1];
        stepLengths[1]          = lambda;
    }

    getLogger()->warn("ArmijoLineSearch(): Armijo line search failed to provide a reduction step.");
    this->convergenceStatus = Status::Unknown;
    return 1.0;
}

//-------------------------------------------------------------------------------------------------

double ArmijoLineSearch::parabolicSearch(const std::array<double, 2> &stepLengths,
                                         const std::array<double, 3> &squaredFunctionNorms) const
{
    auto c21 = stepLengths[0] * (squaredFunctionNorms[2] - squaredFunctionNorms[0]);
    auto c22 = stepLengths[1] * (squaredFunctionNorms[1] - squaredFunctionNorms[0]);

    /// if c2 = c21 - c22 > 0, then we have a concave down curvature and we are done
    /// lambda = this->sigma1*lambda
    auto hi = this->sigma[1] * stepLengths[1];

    if (c21 >= c22)
    {
        getLogger()->info("ArmijoLineSearch::parabolicSearch(): Concave up curvature, stepLength = {0}", hi);
        return hi;
    }

    auto c11    = stepLengths[1] * c22;
    auto c12    = stepLengths[0] * c21;
    auto lambda = -0.5 * (c11 - c12) / (c21 - c22);
    auto lo     = this->sigma[0] * stepLengths[1];

    if (hi < lo)
    {
        getLogger()->critical("ArmijoLineSearch::parabolicSearch(): hi = {0} must be greater than lo = {1}",
                              hi, lo);
        return 0.0;
    }

    // Clamp lambda to the interval [lo,hi]
    return lambda < lo ? lo : lambda > hi ? hi : lambda;
}

//-------------------------------------------------------------------------------------------------

ArmijoLineSearch::Status ArmijoLineSearch::getConvergenceStatus() const { return this->convergenceStatus; }
