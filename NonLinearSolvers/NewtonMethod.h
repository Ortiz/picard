// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <functional>
#include <memory>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

class NewtonMethod : public IterativeMethod<NewtonMethod>
{
public:
    using Self         = NewtonMethod;
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const VectorType &)>;

public:
    explicit NewtonMethod(const FunctionType &f);
    NewtonMethod() = delete;
    NewtonMethod(const NewtonMethod &other);
    NewtonMethod(NewtonMethod &&other);
    NewtonMethod &operator=(const NewtonMethod &other);
    NewtonMethod &operator=(NewtonMethod &&other);

    /**
     * @brief Explicit non-default definition is required.
     */
    ~NewtonMethod();

    void init(const VectorType &x0);

    /**
     * @brief Complete one iteration.
     * @return Reference to object.
     */
    void solve();

    /**
     * @brief Return last iterate
     * @return Latest iteration
     */
    const VectorType &get() const;

    /**
     * @brief Return state of convergence
     * @return enum class Status
     */
    inline Status getConvergenceStatus() const;

private:
    class InexactNewtonMethod;
    std::unique_ptr<InexactNewtonMethod> solver;
};
