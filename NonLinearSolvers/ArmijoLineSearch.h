// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <array>
#include <functional>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

/**
 * @brief Armijo line search method with parabolic model
 */
class ArmijoLineSearch : public IterativeMethod<ArmijoLineSearch>
{
public:
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const VectorType &)>;

public:
    explicit ArmijoLineSearch(const FunctionType &f);
    ArmijoLineSearch() = delete;

    /**
     * @brief Satisfy Armijo line search rule.
     * @param x Current iterate
     * @param dx Direction vector
     * @param functionNorms Rhs function norms
     * @return Accumulation of lambdas
     */
    double operator()(const VectorType &x, const VectorType &dx, std::array<double, 2> &functionNorms);

    /**
     * \brief Three-point safeguarded parabolic model for a line search. Compute coefficients of interpolation
     * polynomial p(lambda) = squaredFunctionNorms + (c1*lambda + c2*lambda^2)/d1,
     * d1 = (stepLengths[0] - stepLengths[1])*stepLengths[0]*stepLengths[1] < 0
     *
     * \param stepLengths[0] Previous steplength
     * \param stepLengths[1] Current steplength
     * \param squaredFunctionNorms[0] Value of |F(xc)|^2
     * \param squaredFunctionNorms[1] Value of |F(xc + stepLengths[0]*d)|^2
     * \param squaredFunctionNorms[2] Value of |F(xc + stepLengths[1]*d)|^2
     * \return New value of lambda given by the parabolic model.
     */
    double parabolicSearch(const std::array<double, 2> &stepLengths,
                           const std::array<double, 3> &squaredFunctionNorms) const;

    /**
     * @brief Return state of convergence
     * @return enum class Status
     */
    Status getConvergenceStatus() const;

private:
    FunctionType objectiveFunction;             // RHS function
    std::array<double, 2> sigma;                // Sigma values
    Status convergenceStatus = Status::Unknown; // Convergence state

    size_t armijoMaxIterations = 100;
    double alpha               = 1e-4;
};
