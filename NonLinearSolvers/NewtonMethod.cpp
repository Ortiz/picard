// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "NewtonMethod.h"

// STL includes
#include <array>

// Picard includes
#include "LinearSolvers/GeneralizedMinimalResidual.h"
#include "NonLinearSolvers/ArmijoLineSearch.h"
#include "Utilities/Timer.h"

class NewtonMethod::InexactNewtonMethod
{
public:
    explicit InexactNewtonMethod(const FunctionType &f);

    template <typename... Args>
    void init(Args &&...args)
    {
        this->initialize(args...);
    }

    /**
     * @brief The actual solve routine.
     * @return Upon return, the solution vector x will be updated.
     */
    void solve();

    /**
     * @brief
     * @return
     */
    VectorType &solution();

    /**
     * @brief Return state of convergence
     * @return enum class Status
     */
    inline Status getConvergenceStatus() const;

private:
    void initialize(const VectorType &x0);

    void initialize(const double alpha, const double sigma0, const double sigma1);

    void initialize(const size_t newtonMaxIterations,
                    const size_t lineSearchIterations,
                    const size_t linearSolverMaxIterations);

    void initialize(const VectorType &x0,
                    const size_t newtonMaxIterations,
                    const size_t lineSearchIterations,
                    const size_t linearSolverMaxIterations);

    void initialize(const size_t newtonMaxIterations,
                    const size_t lineSearchIterations,
                    const size_t linearSolverMaxIterations,
                    const double alpha,
                    const double sigma0,
                    const double sigma1);

    void initialize(const VectorType &x0,
                    const size_t newtonMaxIterations,
                    const size_t lineSearchIterations,
                    const size_t linearSolverMaxIterations,
                    const double alpha,
                    const double sigma0,
                    const double sigma1);

    /**
     * @brief Adjust eta as per Eisenstat-Walker.
     * @param eta
     * @param stopTolerance
     * @param functionNorms
     * @return
     */
    double computeForcingTerm(const double eta,
                              const double stopTolerance,
                              const std::array<double, 2> functionNorms);

private:
    std::shared_ptr<GeneralizedMinimalResidual> linearSolver;
    FunctionType objectiveFunction;
    VectorType x; // Iterate
    VectorType f; // Function values
    size_t newtonMaxIterations;
    size_t lineSearchIterations;
    size_t gmresMaxIterations;
    double alpha;
    double sigma0;
    double sigma1;
    Status convergenceStatus {Status::Unknown}; // Convergence state
};

//-------------------------------------------------------------------------------------------------
// Definitions for NewtonMethod::InexactNewtonMethod
//-------------------------------------------------------------------------------------------------

NewtonMethod::InexactNewtonMethod::InexactNewtonMethod(const FunctionType &function)
    : objectiveFunction(function)
{
    this->linearSolver = GeneralizedMinimalResidual::New(
      [this](const VectorType &dx)
      {
          // Compute approximation to jacobian matrix.
          double eps  = 1.0e-10;
          double norm = dx.norm();

          VectorType j = VectorType::Zero(dx.size());

          if (norm == 0.0)
          {
              getLogger()->warn(
                "NewtonMethod::Jacobian(): Zero or near-zero direction vector dx for jacobian system.");
              return j;
          }

          // Scale the step
          auto xs = x.dot(dx) / norm;

          if (xs != 0.0)
          {
              auto sign = std::signbit(xs) ? -1.0 : 1.0;
              eps *= std::max(std::fabs(xs), 1.0) * sign;
          }

          /// Scale the difference increment
          eps /= norm;
          j = (objectiveFunction(x + eps * dx) - f) / eps;

          if (j.norm() < eps)
          {
              j.setZero();
          }

          return j;
      });

    this->newtonMaxIterations  = 100u;
    this->lineSearchIterations = 20u;
    this->gmresMaxIterations   = 100u;
    this->alpha                = 1e-4;
    this->sigma0               = 0.1;
    this->sigma1               = 0.5;
}

//-------------------------------------------------------------------------------------------------

void NewtonMethod::InexactNewtonMethod::solve()
{
    getLogger()->trace("NewtonMethod::solve(): Evaluating the Intexact Newton method with size: {0}",
                       this->x.size());
    Timer timeThis("NewtonMethod::solve()");

    // Evaluate F at initial iterate and compute the stop tolerance
    this->f = this->objectiveFunction(this->x);

    // Need to save consecutive function norms
    std::array<double, 2> functionNorms = {this->f.norm()};

    // Absolute and relative tolerance
    // TODO: Class parameter
    std::array<double, 2> tolerance = {1e-13, 1e-13};
    double stopTolerance            = tolerance[0] + tolerance[1] * functionNorms[0];

    if (functionNorms[0] < stopTolerance)
    {
        getLogger()->info("NewtonMethod::solve(): Newton method converged with error: {0}", functionNorms[0]);
        this->convergenceStatus = Status::Converged;
        return;
    }

    // TODO: Class parameter
    double gmresTolerance = 0.9;

    ArmijoLineSearch armijo(objectiveFunction);

    for (size_t i = 0; i < this->newtonMaxIterations; ++i)
    {
        // Initialize the iterative linear solver with the zero verctor and solve the system
        this->linearSolver->init(VectorType::Zero(this->x.size()), this->f);
        this->linearSolver->step();

        VectorType dx = this->linearSolver->get();
        auto lambda   = armijo(x, dx, functionNorms);

        this->x -= lambda * dx;
        this->f = this->objectiveFunction(this->x);

        if (functionNorms[1] < stopTolerance)
        {
            getLogger()->info("NewtonMethod::solve(): "
                              "Newton method converged in {0} iterations with error {1}.",
                              i, functionNorms[1]);
            this->convergenceStatus = Status::Converged;
            break;
        }

        gmresTolerance   = this->computeForcingTerm(gmresTolerance, stopTolerance, functionNorms);
        functionNorms[0] = functionNorms[1];
    }

    if (functionNorms[1] > stopTolerance)
    {
        getLogger()->error("Newton method failed to converge to desired accuracy. Function norm = {0}",
                           functionNorms[1]);

        this->convergenceStatus = Status::Diverged;
    }
}

//-------------------------------------------------------------------------------------------------

inline NewtonMethod::VectorType &NewtonMethod::InexactNewtonMethod::solution() { return this->x; }

//-------------------------------------------------------------------------------------------------

inline void NewtonMethod::InexactNewtonMethod::initialize(const VectorType &x0)
{
    this->x = x0;
    this->f.resize(x0.size());
}

//-------------------------------------------------------------------------------------------------

inline void
NewtonMethod::InexactNewtonMethod::initialize(const double alpha, const double sigma0, const double sigma1)
{
    this->alpha  = alpha;
    this->sigma0 = sigma0;
    this->sigma1 = sigma1;
}

//-------------------------------------------------------------------------------------------------

inline void NewtonMethod::InexactNewtonMethod::initialize(const size_t newtonMaxIterations,
                                                          const size_t lineSearchIterations,
                                                          const size_t linearSolverMaxIterations)
{
    this->newtonMaxIterations  = newtonMaxIterations;
    this->lineSearchIterations = lineSearchIterations;
    this->gmresMaxIterations   = linearSolverMaxIterations;
}

//-------------------------------------------------------------------------------------------------

inline void NewtonMethod::InexactNewtonMethod::initialize(const VectorType &x0,
                                                          const size_t newtonMaxIterations,
                                                          const size_t lineSearchIterations,
                                                          const size_t linearSolverMaxIterations)
{
    this->initialize(x0);
    this->initialize(newtonMaxIterations, lineSearchIterations, linearSolverMaxIterations);
}

//-------------------------------------------------------------------------------------------------

inline void NewtonMethod::InexactNewtonMethod::initialize(const size_t newtonMaxIterations,
                                                          const size_t lineSearchIterations,
                                                          const size_t linearSolverMaxIterations,
                                                          const double alpha,
                                                          const double sigma0,
                                                          const double sigma1)
{
    this->initialize(newtonMaxIterations, lineSearchIterations, linearSolverMaxIterations);
    this->initialize(alpha, sigma0, sigma1);
}

//-------------------------------------------------------------------------------------------------

double NewtonMethod::InexactNewtonMethod::computeForcingTerm(const double eta,
                                                             const double stopTolerance,
                                                             const std::array<double, 2> functionNorms)
{
    // TODO: Class parameter?
    double gamma = 0.9;

    if (functionNorms[0] == 0.0 || functionNorms[1] == 0.0)
    {
        getLogger()->info("NewtonMethod::computeForcingTerm(): Function norm reached zero value.");
        return gamma;
    }

    // Compute ratio of successive residual norms and iteration counter
    double normRatio = functionNorms[1] / functionNorms[0];

    std::array<double, 2> etanew = {gamma * normRatio * normRatio, gamma * eta * eta};

    if (etanew[1] > .1)
    {
        etanew[0] = std::max(etanew[0], etanew[1]);
    }

    double newTolerance = std::max(std::min(etanew[0], eta), 0.5 * stopTolerance / functionNorms[1]);

    getLogger()->info("NewtonMethod::computeForcingTerm(): "
                      "Updated linear solver tolerance to {0}",
                      newTolerance);

    return newTolerance;
}

//-------------------------------------------------------------------------------------------------

inline NewtonMethod::Status NewtonMethod::InexactNewtonMethod::getConvergenceStatus() const
{
    return this->convergenceStatus;
}

//-------------------------------------------------------------------------------------------------
// Definitions for NewtonMethod
//-------------------------------------------------------------------------------------------------

NewtonMethod::NewtonMethod(const FunctionType &f) : solver(new InexactNewtonMethod(f)) { }

//-------------------------------------------------------------------------------------------------

NewtonMethod::NewtonMethod(const NewtonMethod &other) : solver(new InexactNewtonMethod(*other.solver))
{
    getLogger()->trace("NewtonMethod::NewtonMethod(NewtonMethod &other)");
}

//-------------------------------------------------------------------------------------------------

NewtonMethod::NewtonMethod(NewtonMethod &&other) : solver(std::move(other.solver))
{
    getLogger()->trace("NewtonMethod::NewtonMethod(NewtonMethod &&other)");
}

//-------------------------------------------------------------------------------------------------

NewtonMethod &NewtonMethod::operator=(const NewtonMethod &other)
{
    getLogger()->trace("NewtonMethod::operator=(const NewtonMethod &other)");
    this->solver.reset(new InexactNewtonMethod(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

NewtonMethod &NewtonMethod::operator=(NewtonMethod &&other)
{
    getLogger()->trace("NewtonMethod::operator=(NewtonMethod &&other)");
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

NewtonMethod::~NewtonMethod() { }

//-------------------------------------------------------------------------------------------------

void NewtonMethod::init(const VectorType &x0) { this->solver->init(x0); }

//-------------------------------------------------------------------------------------------------

void NewtonMethod::solve() { this->solver->solve(); }

//-------------------------------------------------------------------------------------------------

const NewtonMethod::VectorType &NewtonMethod::get() const { return this->solver->solution(); }

//-------------------------------------------------------------------------------------------------

NewtonMethod::Status NewtonMethod::getConvergenceStatus() const
{
    return this->solver->getConvergenceStatus();
}
