// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "ArnoldiIteration.h"

ArnoldiIteration::ArnoldiIteration(const double tol) : tolerance(tol) { }

//-------------------------------------------------------------------------------------------------

void ArnoldiIteration::operator()(MatrixType &v, MatrixType &h, const int k)
{
    Timer("Arnoldi");
    double vNorm = v.col(k + 1).norm();

    // No need to continue since no more orthogonal vector can be generated.
    if (vNorm == 0.0 && k == 0)
    {
        h(0, 0) = v.col(0).norm();
        return;
    }

    /// Apply Arnoldi's method with modified Gram-Schmidt orthogonalization to v(k+1)
    for (size_t i = 0; i <= k; ++i)
    {
        h(i, k) = v.col(i).dot(v.col(k + 1));
        v.col(k + 1) -= h(i, k) * v.col(i);
    }

    h(k + 1, k) = v.col(k + 1).norm();

    if (h(k + 1, k) == 0.0)
    {
        getLogger()->warn("No more orthogonal vectors possible. |v({0})| = {1}.", k + 1, h(k + 1, k));
        return;
    }

    /// Re-orthogonalized if necessary
    if (vNorm + this->tolerance * h(k + 1, k) == vNorm)
    {
        getLogger()->warn("Reorthogonalizing. H({0},{1}) = {2}.", k + 1, k, h(k + 1, k));

        for (size_t i = 0; i < k + 1; ++i)
        {
            double hr = v.col(i).dot(v.col(k + 1));
            h(i, k) += hr;
            v.col(k + 1) -= hr * v.col(i);
        }

        h(k + 1, k) = v.col(k + 1).norm();
    }

    /// Normalize
    v.col(k + 1) /= h(k + 1, k);
}
