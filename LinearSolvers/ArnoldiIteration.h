// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"
#include "Utilities/Timer.h"

/**
 * @brief Orthonormalization method.
 */
class ArnoldiIteration : public IterativeMethod<ArnoldiIteration>
{
public:
    using MatrixType = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

public:
    explicit ArnoldiIteration(const double tol);

    /**
     * @brief Orthonormalization method.  Applies Arnoldi's method with modified Gram-Schmidt
     *   orthogonalization to the columns of v
     * @param k iteration index
     * @return Upon return, h(k+1,k) = norm(v(:,k+1)) and v(:,k+1) will be
     *   orthonormal to all previous columns
     */
    void operator()(MatrixType &v, MatrixType &h, const int k);

private:
    double tolerance; // Re-orthogonalize if vector norms are smaller than this value
};
