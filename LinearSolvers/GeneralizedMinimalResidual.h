// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <memory>

// External dependencies
#include <Eigen/Core>

// Picard includes
#include "Core/IterativeMethod.hpp"

class GeneralizedMinimalResidual : public IterativeMethod<GeneralizedMinimalResidual>
{
public:
    using Self         = GeneralizedMinimalResidual;
    using VectorType   = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using FunctionType = std::function<VectorType(const VectorType &)>;

public:
    explicit GeneralizedMinimalResidual(const FunctionType &f);

    GeneralizedMinimalResidual() = delete;

    GeneralizedMinimalResidual(const GeneralizedMinimalResidual &other);

    GeneralizedMinimalResidual(GeneralizedMinimalResidual &&other);

    GeneralizedMinimalResidual &operator=(const GeneralizedMinimalResidual &other);

    GeneralizedMinimalResidual &operator=(GeneralizedMinimalResidual &&other);

    /**
     * @brief Explicit non-default definition is required.
     */
    ~GeneralizedMinimalResidual();

    /**
     * @brief Set initial vector and right hand side.
     * @param x0 Initial vector
     * @param b Right hand side
     * @return (void)
     */
    void init(const VectorType &x0, const VectorType &b);

    void step();

    /**
     * @brief Returns the last iterate
     * @return Vector containing approximate solution
     */
    const VectorType &get() const;

    /**
     * @brief Return state of convergence
     * @return enum class Status
     */
    Status getConvergenceStatus() const;

private:
    class GMRESLinearSolver;
    std::unique_ptr<GMRESLinearSolver> solver;
};
