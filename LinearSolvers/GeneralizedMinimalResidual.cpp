// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "GeneralizedMinimalResidual.h"

// External dependencies
#include <Eigen/Jacobi>

// Picard includes
#include "LinearSolvers/ArnoldiIteration.h"

class GeneralizedMinimalResidual::GMRESLinearSolver
{
public:
    using MatrixType = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

public:
    /**
     * @brief Generalized Minimal Residual Method.  Solves the linear system:  A(x)= b, where A(x) = A*x.
     * @param f Matrix operator
     */
    explicit GMRESLinearSolver(const FunctionType &f);

    void init(const VectorType &x0, const VectorType &b);

    void iterate();

    /**
     * @brief Return last iteration
     * @return Latest iteration
     */
    VectorType &solution();

    /**
     * @brief Return state of convergence
     * @return enum class Status
     */
    inline Status getConvergenceStatus() const;

private:
    size_t krylovSpaceSize;
    double errtol;
    size_t maxIters;
    std::vector<Eigen::JacobiRotation<double>> givensRotations; // Givens rotations vector
    FunctionType A;                                             // Matrix operator
    VectorType x;                                               // Iterates
    VectorType b;                                               // Right-hand side
    MatrixType h;                                               // Hessemberg matrix
    MatrixType v;                                               // Orthogonalized columns vectors
    VectorType g;                                               // Auxiliary vector
    Status convergenceStatus = Status::Unknown;                 // Convergence state
};

//-------------------------------------------------------------------------------------------------
// Definitions for GeneralizedMinimalResidual::GMRESLinearSolver
//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::GMRESLinearSolver::GMRESLinearSolver(const FunctionType &f) : A(f)
{
    getLogger()->trace("Instantiating the GMRES method.");
    this->krylovSpaceSize = 1u;
    this->errtol          = 1.0e-13;
    this->maxIters        = 8000;
}

//-------------------------------------------------------------------------------------------------

void GeneralizedMinimalResidual::GMRESLinearSolver::init(const VectorType &x0, const VectorType &b)
{
    getLogger()->trace("init() - Initial conditions, x0.size() = {1}, b.size() = {0}", x0.size(), b.size());
    this->krylovSpaceSize = b.size();
    this->errtol          = 1.0e-13;
    this->maxIters        = 8000;
    this->x               = x0;
    this->b               = b;
    this->v               = MatrixType::Zero(b.size(), this->krylovSpaceSize + 1);
    this->h               = MatrixType::Zero(this->krylovSpaceSize + 1, this->krylovSpaceSize);
    this->g               = VectorType::Zero(this->krylovSpaceSize + 1);
    this->givensRotations.resize(this->krylovSpaceSize);
}

//-------------------------------------------------------------------------------------------------

void GeneralizedMinimalResidual::GMRESLinearSolver::iterate()
{
    getLogger()->trace("GeneralizedMinimalResidualMethod::iterate(): "
                       "Evaluating the GMRES method with Krylov subspace of size: {0}",
                       this->krylovSpaceSize);

    /// Compute the residual
    this->v.col(0) = b - A(x);
    this->g(0)     = v.col(0).norm();

    if (std::isinf(this->g(0)))
    {
        getLogger()->critical("GeneralizedMinimalResidualMethod::iterate(): "
                              "Unbounded residual. Early termination.");

        this->convergenceStatus = Status::Diverged;
        return;
    }

    auto bNorm     = b.norm();
    auto tolerance = this->errtol * b.norm();
    getLogger()->info("GeneralizedMinimalResidualMethod::iterate(): "
                      "Computed tolerance: tol = errtol * |b| = {0} * {1} = {2}.",
                      this->errtol, bNorm, tolerance);

    /// Test for early termination
    if (this->g(0) < tolerance)
    {
        getLogger()->info("GeneralizedMinimalResidualMethod::iterate(): "
                          "Method converged. Residual norm: {0}, total iterations: 0",
                          this->g(0));
        this->convergenceStatus = Status::Converged;
        return;
    }

    this->v.col(0) /= this->g(0);

    /// Start GMRES iterations
    int k             = 0;
    int iterations    = 0;
    double reOrthoTol = 0.001;
    ArnoldiIteration arnoldi(reOrthoTol);

    for (k = 0; k < this->krylovSpaceSize; ++k)
    {
        ++iterations;

        /// Evaluate linear operator
        this->v.col(k + 1) = this->A(v.col(k));

        /// Apply Arnoldi's method with modified Gram-Schmidt orthogonalization to the columns of v
        arnoldi(this->v, this->h, k);

        // Exit early if a nan is encountered.
        if (std::isnan(this->h(k + 1, k)))
        {
            getLogger()->critical(
              "GeneralizedMinimalResidualMethod::iterate(): "
              "nan encountered. Total iterations: {0}, Krylov subspace dimensions used: {1}",
              iterations, k + 1);
            this->convergenceStatus = Status::Diverged;
            return;
        }

        /// Perform Givens rotations in order to eliminate the h(k+1,k) entry from the
        /// Hessenberg matrix.
        for (int i = 0; i < k; ++i)
        {
            this->h.col(k).applyOnTheLeft(i, i + 1, this->givensRotations[i].adjoint());
        }

        // Compute the Given's rotation matrix
        this->givensRotations[k].makeGivens(this->h(k, k), this->h(k + 1, k));

        // Apply the Givens rotation matrix to h and g
        this->h.col(k).applyOnTheLeft(k, k + 1, this->givensRotations[k].adjoint());
        this->g.applyOnTheLeft(k, k + 1, this->givensRotations[k].adjoint());

        // Warn if Givens rotation fails.
        if (this->h(k + 1, k) > std::numeric_limits<float>::epsilon())
        {
            getLogger()->error("GeneralizedMinimalResidualMethod::iterate(): "
                               "Givens rotation failed. H({0},{1}) = {2}.",
                               k + 1, k, this->h(k + 1, k));
        }

        /// The norm of the residual is the last entry in g
        if (std::fabs(this->g(k + 1)) < tolerance)
        {
            getLogger()->info("GeneralizedMinimalResidualMethod::iterate(): "
                              "Method converged. Residual norm: {0}, total iterations: {1}, Krylov subspace "
                              "dimensions used: {2}",
                              this->g(k + 1), iterations, k + 1);
            this->convergenceStatus = Status::Converged;
            break;
        }

        /// Test for early termination
        if (k == this->b.size() || iterations == this->maxIters)
        {
            getLogger()->error("GeneralizedMinimalResidualMethod::iterate(): "
                               "Method did not converge. Residual norm: {0}, total iterations: {1}, Krylov "
                               "subspace dimensions used: {2}",
                               this->g(k + 1), iterations, k + 1);
            this->convergenceStatus = Status::Diverged;
            break;
        }

        // Restart iterations
        if (k == this->krylovSpaceSize - 1)
        {
            getLogger()->info("GeneralizedMinimalResidualMethod::iterate(): Maximum number of "
                              "iterations reached. Residual norm: {0}, total iterations: {1}, restarting...",
                              this->g(k + 1), iterations);

            Eigen::Ref<VectorType> y = this->g.head(k + 1);
            this->h.topLeftCorner(k + 1, k + 1).template triangularView<Eigen::Upper>().solveInPlace(y);

            for (size_t i = 0; i < k + 1; ++i)
            {
                this->x += y(i) * this->v.col(i);
            }

            k       = -1;
            this->v = MatrixType::Zero(b.size(), this->krylovSpaceSize + 1);
            this->h = MatrixType::Zero(this->krylovSpaceSize + 1, this->krylovSpaceSize);
            this->g = VectorType::Zero(this->krylovSpaceSize + 1);

            this->v.col(0) = this->b - this->A(this->x);
            this->g(0)     = this->v.col(0).norm();
            this->v.col(0) *= 1.0 / g(0);
        }
        getLogger()->info("GeneralizedMinimalResidualMethod::iterate(): Completed one iteration "
                          "Residual norm: {0}, total iterations: {1}",
                          this->g(k + 1), iterations);
    }

    Eigen::Ref<VectorType> y = this->g.head(k + 1);
    this->h.topLeftCorner(k + 1, k + 1).template triangularView<Eigen::Upper>().solveInPlace(y);
    for (size_t i = 0; i < k + 1; ++i)
    {
        this->x += y(i) * v.col(i);
    }
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::VectorType &GeneralizedMinimalResidual::GMRESLinearSolver::solution()
{
    return this->x;
}

//-------------------------------------------------------------------------------------------------

inline GeneralizedMinimalResidual::Status
GeneralizedMinimalResidual::GMRESLinearSolver::getConvergenceStatus() const
{
    return this->convergenceStatus;
}

//-------------------------------------------------------------------------------------------------
// Definitions for GeneralizedMinimalResidual
//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::GeneralizedMinimalResidual(const FunctionType &f)
    : solver(new GMRESLinearSolver(f))
{
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::GeneralizedMinimalResidual(const GeneralizedMinimalResidual &other)
    : solver(new GMRESLinearSolver(*other.solver))
{
    getLogger()->trace(
      "GeneralizedMinimalResidual::GeneralizedMinimalResidual(GeneralizedMinimalResidual &other)");
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::GeneralizedMinimalResidual(GeneralizedMinimalResidual &&other)
    : solver(std::move(other.solver))
{
    getLogger()->trace(
      "GeneralizedMinimalResidual::GeneralizedMinimalResidual(GeneralizedMinimalResidual &&other)");
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual &GeneralizedMinimalResidual::operator=(const GeneralizedMinimalResidual &other)
{
    getLogger()->trace("GeneralizedMinimalResidual::operator=(GeneralizedMinimalResidual &other)");
    this->solver.reset(new GMRESLinearSolver(*other.solver));
    return *this;
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual &GeneralizedMinimalResidual::operator=(GeneralizedMinimalResidual &&other)
{
    getLogger()->trace("GeneralizedMinimalResidual::operator=(GeneralizedMinimalResidual &&other)");
    this->solver = std::move(other.solver);
    return *this;
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::~GeneralizedMinimalResidual() { }

//-------------------------------------------------------------------------------------------------

void GeneralizedMinimalResidual::init(const VectorType &x0, const VectorType &b)
{
    this->solver->init(x0, b);
}

//-------------------------------------------------------------------------------------------------

void GeneralizedMinimalResidual::step() { this->solver->iterate(); }

//-------------------------------------------------------------------------------------------------

const GeneralizedMinimalResidual::VectorType &GeneralizedMinimalResidual::get() const
{
    return this->solver->solution();
}

//-------------------------------------------------------------------------------------------------

GeneralizedMinimalResidual::Status GeneralizedMinimalResidual::getConvergenceStatus() const
{
    return this->solver->getConvergenceStatus();
}
