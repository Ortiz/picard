add_library(LinearSolvers GeneralizedMinimalResidual.cpp ArnoldiIteration.cpp)
target_link_libraries(
  LinearSolvers PRIVATE Eigen3::Eigen spdlog::spdlog Picard::TestCoverage
)

target_include_directories(
  LinearSolvers
  PRIVATE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
          $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  INTERFACE $<INSTALL_INTERFACE:include/Picard>
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
            $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
)

generate_export_header(
  LinearSolvers
  EXPORT_FILE_NAME
  ${CMAKE_CURRENT_BINARY_DIR}/LinearSolversExports.h
  EXPORT_MACRO_NAME
  LINEARSOLVERS_API
  PREFIX_NAME
  PICARD_
)

# Export symbols building.
set_target_properties(
  LinearSolvers PROPERTIES DEFINE_SYMBOL "LinearSolvers_EXPORTS"
)

install(TARGETS LinearSolvers EXPORT PicardTargets)

install(
  DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
  DESTINATION "include/Picard"
  FILES_MATCHING
  PATTERN "*.h"
)

install(
  DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
  DESTINATION "include/Picard"
  FILES_MATCHING
  PATTERN CMakeFiles EXCLUDE
  PATTERN "*.h"
)
