# Coverage target. Link against this target to enable test coverage report.
if(NOT TARGET Picard::TestCoverage)
  add_library(Picard::TestCoverage INTERFACE IMPORTED)
endif()

option(ENABLE_COVERAGE "Enable coverage reporting" ON)
if(ENABLE_COVERAGE)
  # I presume this might also be clang's options but for now only works for GCC
  target_compile_options(
    Picard::TestCoverage INTERFACE $<$<CXX_COMPILER_ID:GNU>:-g -O0 --coverage>
  )
  if(CMAKE_VERSION VERSION_GREATER_EQUAL 3.13)
    target_link_options(
      Picard::TestCoverage INTERFACE $<$<CXX_COMPILER_ID:GNU>:--coverage>
    )
  else()
    target_link_libraries(
      Picard::TestCoverage INTERFACE $<$<CXX_COMPILER_ID:GNU>:--coverage>
    )
  endif()
endif()
