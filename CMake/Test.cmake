# Overload of the add_test function. This function creates a test driver for
# each test and allows to run a group of tests under same driver.
function(add_test)
  set(options "")
  set(one_val_options DRIVER_NAME WORKING_DIRECTORY)
  set(multi_val_options SOURCES)
  cmake_parse_arguments(
    TEST "${options}" "${one_val_options}" "${multi_val_options}" ${ARGN}
  )

  if(NOT TEST_DRIVER_NAME)
    message(FATAL_ERROR "Test with no name.")
  endif()

  if(NOT TEST_WORKING_DIRECTORY)
    set(TEST_WORKING_DIRECTORY $<TARGET_FILE_DIR:${TEST_DRIVER_NAME}>)
  endif()

  # Create a test driver and get all the sources under the variable
  # TestDriverSources
  create_test_sourcelist(
    TestDriverSources ${TEST_DRIVER_NAME}Driver.cpp ${TEST_SOURCES}
  )

  # The actual test executable
  add_executable(${TEST_DRIVER_NAME} ${TestDriverSources})
  target_include_directories(${TEST_DRIVER_NAME} PRIVATE ${PROJECT_SOURCE_DIR})

  # Loop through all the test files and create a CTest for each one
  foreach(src ${TEST_SOURCES})
    get_filename_component(test_src ${src} NAME_WLE)
    _add_test(
      NAME ${test_src}
      COMMAND $<TARGET_FILE:${TEST_DRIVER_NAME}> ${test_src}
      WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY}
    )

  endforeach()
endfunction(add_test)

# Find gzip and create function-process to unzip files
find_program(GUNZIP_EXECUTABLE gzip)
function(unzip file out_path)
  if(GUNZIP_EXECUTABLE)
    get_filename_component(output_file ${file} NAME_WLE)
    execute_process(
      COMMAND ${GUNZIP_EXECUTABLE} -dcf ${file}
      OUTPUT_FILE ${output_file}
      WORKING_DIRECTORY ${out_path}
    )

  else()
    message(WARNING "GUNZIP_EXECUTABLE is not defined. Failed to unzip file.")
  endif()
endfunction(unzip)

# Simple download/extract function
function(download out_path)
  foreach(url ${ARGN})
    get_filename_component(output_file ${url} NAME)
    file(DOWNLOAD ${url} ${out_path}/${output_file})
    # Unzip, if possible, into data directory
    get_filename_component(output_ext ${output_file} LAST_EXT)
    if(output_ext STREQUAL ".gz")
      file(MAKE_DIRECTORY ${out_path}/data)
      unzip(${out_path}/${output_file} ${out_path}/data)
    endif()
  endforeach()
endfunction(download)
