# ==============================================================================
# Export directives for development of external applications and packaging
# ------------------------------------------------------------------------------
include(CMakePackageConfigHelpers)

set(CPACK_PACKAGE_NAME "Picard")
set(CPACK_PACKAGE_VERSION_MAJOR ${Picard_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${Picard_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${Picard_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION ${Picard_VERSION})
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
    "Picard - Time stepping methods for stiff IVPs."
)
set(CPACK_RPM_PACKAGE_VENDOR ${CPACK_PACKAGE_VENDOR})
set(CPACK_RPM_PACKAGE_URL "http://gitlab.com/ortiz/picard")
set(CPACK_RPM_DEFAULT_FILE_PERMISSIONS
    OWNER_READ
    OWNER_WRITE
    OWNER_EXECUTE
    GROUP_READ
    GROUP_EXECUTE
    WORLD_READ
    WORLD_EXECUTE
)
set(CPACK_RPM_DEFAULT_DIR_PERMISSIONS
    OWNER_READ
    OWNER_WRITE
    OWNER_EXECUTE
    GROUP_READ
    GROUP_EXECUTE
    WORLD_READ
    WORLD_EXECUTE
)
set(CPACK_RPM_PACKAGE_AUTOREQPROV 0)
set(CPACK_RPM_PACKAGE_ARCHITECTURE x86_64)
set(CPACK_RPM_PACKAGE_RELOCATABLE ON)
set(CPACK_RPM_PACKAGE_RELEASE_DIST ON)
set(CPACK_RPM_PACKAGE_LICENSE "MIT License")
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_SOURCE_DIR}/LICENSE.md)
set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION
    ${CPACK_PACKAGING_INSTALL_PREFIX}
)

set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
set(CPACK_RPM_FILE_NAME RPM-DEFAULT)
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Picard Devs")

# These are applied to all platforms and setting these stops the CPack.cmake
# script from generating options about other package compression formats (.z
# .tz, etc.)
set(PICARD_CPACK_GENERATOR
    "TGZ"
    CACHE STRING "CPack Generator: RPM, TGZ, RPM;TGZ, etc"
)

if(MSVC)
  set(CPACK_GENERATOR "NSIS")
else()
  set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
  set(CPACK_GENERATOR ${PICARD_CPACK_GENERATOR})
endif()

configure_package_config_file(
  CMake/PicardConfig.cmake.in "${CMAKE_BINARY_DIR}/PicardConfig.cmake"
  INSTALL_DESTINATION lib/Picard/cmake
  NO_CHECK_REQUIRED_COMPONENTS_MACRO
)

write_basic_package_version_file(
  "${CMAKE_BINARY_DIR}/PicardConfigVersion.cmake"
  COMPATIBILITY SameMajorVersion
)

install(FILES ${CMAKE_BINARY_DIR}/PicardConfig.cmake
              ${CMAKE_BINARY_DIR}/PicardConfigVersion.cmake
        DESTINATION lib/Picard/cmake
)

install(
  EXPORT PicardTargets
  FILE PicardTargets.cmake
  NAMESPACE Picard::
  DESTINATION lib/Picard/cmake
)

export(PACKAGE Picard)
export(
  TARGETS LinearSolvers NonLinearSolvers ODESolvers Utilities
  FILE "${CMAKE_BINARY_DIR}/PicardTargets.cmake"
  NAMESPACE Picard::
  EXPORT_LINK_INTERFACE_LIBRARIES
)
