# * Try to find Eigen3 Once done this will define:
#
# EIGEN3_FOUND - system has Eigen3 EIGEN3_INCLUDE_DIR - the Eigen3 include
# directory
#
# Imported target
#
# Eigen3::Eigen

# If we are using conan, then there is a change the targets are already defined,
# use those.
if(USE_CONAN)
  if(TARGET CONAN_PKG::eigen)
    if(NOT TARGET Eigen3::Eigen)
      add_library(Eigen3::Eigen ALIAS CONAN_PKG::eigen)
      return()
    endif()
  endif()
endif(USE_CONAN)

# ==============================================================================
# Search the include directory
# ==============================================================================
find_path(
  EIGEN3_INCLUDE_DIR signature_of_eigen3_matrix_library
  PATHS "${Eigen3_DIR}"
  PATH_SUFFIXES eigen3
)

# ==============================================================================
# Standard epilogue
# ------------------------------------------------------------------------------
# Handle the QUIETLY and REQUIRED arguments and set EIGEN3_FOUND to TRUE if all
# listed variables are TRUE
# ==============================================================================
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Eigen DEFAULT_MSG EIGEN3_INCLUDE_DIR)

if(Eigen_FOUND)
  if(NOT TARGET Eigen3::Eigen)
    add_library(Eigen3::Eigen INTERFACE IMPORTED)
    set_target_properties(
      Eigen3::Eigen PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
                               "${EIGEN3_INCLUDE_DIR}"
    )
  endif()
endif()

mark_as_advanced(EIGEN3_INCLUDE_DIR)
