option(USE_CONAN "Use conan to resolve dependencies." ON)
if(USE_CONAN)
  # Download connan utilities automatically.
  if(NOT EXISTS "${CMAKE_BINARY_DIR}/CMake/conan.cmake")
    message(
      STATUS
        "Downloading conan.cmake from https://github.com/conan-io/cmake-conan"
    )
    file(
      DOWNLOAD
      "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
      "${CMAKE_BINARY_DIR}/CMake/conan.cmake"
    )
  endif()

  include(${PROJECT_BINARY_DIR}/CMake/conan.cmake)

  conan_cmake_run(
    BASIC_SETUP
    REQUIRES spdlog/1.8.5 eigen/3.3.9
    BUILD missing
    CMAKE_TARGETS
  )
endif(USE_CONAN)
