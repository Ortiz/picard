function(write_api_test)

  set(PICARD_SOURCE_DIR ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/../)

  file(GLOB TEST_SOURCES ${PICARD_SOURCE_DIR}/Testing/*.cpp
       ${PICARD_SOURCE_DIR}/Testing/*.h
  )

  file(MAKE_DIRECTORY ${APITEST_DIR}/CMake)

  foreach(src ${TEST_SOURCES})
    configure_file(${src} ${APITEST_DIR}/ COPYONLY)
  endforeach()

  configure_file(
    ${PICARD_SOURCE_DIR}/CMake/FindSpdlog.cmake ${APITEST_DIR}/CMake/ COPYONLY
  )

  configure_file(
    ${PICARD_SOURCE_DIR}/CMake/FindEigen.cmake ${APITEST_DIR}/CMake/ COPYONLY
  )

  configure_file(
    ${PICARD_SOURCE_DIR}/CMake/Conan.cmake ${APITEST_DIR}/CMake/ COPYONLY
  )

  file(
    WRITE ${APITEST_DIR}/main.cpp
    [=[
#include <cstdlib>
int TestBackwardEuler(int ac, char **av);
int TestClenshawCurtis(int ac, char **av);
int TestExplicitSDC(int ac, char **av);
int TestForwardEuler(int ac, char **av);
int TestGeneralizedMinimalResidual(int ac, char **av);
int TestLogger(int ac, char **av);
int TestNewtonMethod(int ac, char **av);
int TestSemiImplicitSDC(int ac, char **av);
int TestTimer(int ac, char **av);
int TestArmijoLineSearch(int ac, char **av);
int TestArnoldiIteration(int ac, char **av);

int main(int ac, char **av)
{
  TestBackwardEuler(ac, av);
  TestClenshawCurtis(ac, av);
  TestExplicitSDC(ac, av);
  TestForwardEuler(ac, av);
  TestGeneralizedMinimalResidual(ac, av);
  TestLogger(ac, av);
  TestNewtonMethod(ac, av);
  TestSemiImplicitSDC(ac, av);
  TestTimer(ac, av);
  TestArmijoLineSearch(ac, av);
  TestArnoldiIteration(ac, av);
  exit(0);
}]=]
  )

  file(
    WRITE ${APITEST_DIR}/CMakeLists.txt
    [=[
cmake_minimum_required(VERSION 3.12)
project(PicardTestAPI)

list(INSERT CMAKE_MODULE_PATH 0 "${PROJECT_SOURCE_DIR}/CMake")

include(Conan)

find_package(Eigen 3.3.9 REQUIRED)
find_package(Spdlog 1.8.5 REQUIRED)
find_package(Picard REQUIRED)

file(GLOB PICARD_TEST_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_executable(main ${PICARD_TEST_SOURCES})
target_link_libraries(main
  PRIVATE
    Picard::LinearSolvers
    Picard::NonLinearSolvers
    Picard::ODESolvers
    Picard::Utilities
    Eigen3::Eigen
    spdlog::spdlog
)]=]
  )

endfunction()

write_api_test()
