# * Try to find spdlog Once done this will define:
#
# Spdlog_FOUND - system has Spdlog SPDLOG_INCLUDE_DIR - the Spdlog include
# directory
#
# Imported target
#
# spdlog::spdlog

# If we are using conan, then there is a change the targets are already defined,
# use those.
if(USE_CONAN)
  if(TARGET CONAN_PKG::spdlog)
    if(NOT TARGET spdlog::spdlog)
      add_library(spdlog::spdlog ALIAS CONAN_PKG::spdlog)
      if(TARGET CONAN_PKG::fmt)
        target_link_libraries(CONAN_PKG::spdlog INTERFACE CONAN_PKG::fmt)
      endif()
    endif()
    return()
  endif()
endif(USE_CONAN)

# ==============================================================================
# Search the include directory
# ==============================================================================
find_path(SPDLOG_INCLUDE_DIR spdlog/spdlog.h PATH_SUFFIXES include)
find_path(FMT_INCLUDE_DIR fmt/core.h PATH_SUFFIXES include)

find_library(SPDLOG_LIBRARY_RELEASE NAMES spdlog)
find_library(SPDLOG_LIBRARY_DEBUG NAMES spdlogd)
find_library(FMT_LIBRARY_RELEASE NAMES fmt)
find_library(FMT_LIBRARY_DEBUG NAMES fmtd)

# ==============================================================================
# Standard epilogue
# ------------------------------------------------------------------------------
# Handle the QUIETLY and REQUIRED arguments and set SPDLOG_FOUND to TRUE if all
# listed variables are TRUE
# ==============================================================================
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  Spdlog DEFAULT_MSG SPDLOG_INCLUDE_DIR FMT_INCLUDE_DIR
)

if(Spdlog_FOUND)
  if(NOT TARGET spdlog::spdlog)
    add_library(spdlog::spdlog INTERFACE IMPORTED)

    if(EXISTS ${SPDLOG_LIBRARY_RELEASE})
      set_property(
        TARGET spdlog::spdlog
        APPEND
        PROPERTY IMPORTED_CONFIGURATIONS RELEASE
      )
      set_target_properties(
        spdlog::spdlog
        PROPERTIES MAP_IMPORTED_CONFIG_RELEASE Release
                   IMPORTED_LOCATION_RELEASE "${SPDLOG_LIBRARY_RELEASE}"
      )
    endif()

    if(EXISTS ${SPDLOG_LIBRARY_DEBUG})
      set_property(
        TARGET spdlog::spdlog
        APPEND
        PROPERTY IMPORTED_CONFIGURATIONS DEBUG
      )
      set_target_properties(
        spdlog::spdlog
        PROPERTIES MAP_IMPORTED_CONFIG_DEBUG Debug IMPORTED_LOCATION_DEBUG
                                                   "${SPDLOG_LIBRARY_DEBUG}"
      )
    endif()

    if(EXISTS ${FMT_LIBRARY_RELEASE})
      set_property(
        TARGET spdlog::spdlog
        APPEND
        PROPERTY IMPORTED_CONFIGURATIONS RELEASE
      )
      set_target_properties(
        spdlog::spdlog
        PROPERTIES MAP_IMPORTED_CONFIG_RELEASE Release IMPORTED_LOCATION_RELEASE
                                                       "${FMT_LIBRARY_RELEASE}"
      )
    endif()

    if(EXISTS ${FMT_LIBRARY_DEBUG})
      set_property(
        TARGET spdlog::spdlog
        APPEND
        PROPERTY IMPORTED_CONFIGURATIONS DEBUG
      )
      set_target_properties(
        spdlog::spdlog
        PROPERTIES MAP_IMPORTED_CONFIG_DEBUG Debug IMPORTED_LOCATION_DEBUG
                                                   "${FMT_LIBRARY_DEBUG}"
      )
    endif()

    set_target_properties(
      spdlog::spdlog PROPERTIES INTERFACE_INCLUDE_DIRECTORIES
                                "${SPDLOG_INCLUDE_DIR};${FMT_INCLUDE_DIR}"
    )
    if(CONAN_COMPILE_DEFINITIONS_SPDLOG)
      target_compile_definitions(
        spdlog::spdlog INTERFACE ${CONAN_COMPILE_DEFINITIONS_SPDLOG}
      )
    endif()
  endif()
endif()

mark_as_advanced(SPDLOG_INCLUDE_DIR)
