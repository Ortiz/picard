// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

#include "ODESolvers/SDC/ExplicitSDC.h"
#include "Utilities.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestExplicitSDC(int, char **)
{
    using VectorType = ExplicitSDC::VectorType;

    // Cover construction and assignament
    {
        auto stepper  = ExplicitSDC::New(0,
                                        [](const double t, const VectorType &x)
                                        {
                                            return x;
                                        });
        auto stepper0 = ExplicitSDC(*stepper);
        auto stepper1 = ExplicitSDC(std::move(stepper0));

        stepper0 = stepper1;
        stepper1 = std::move(stepper0);
    }

    if (TestStiffEquation<ExplicitSDC>(0.001, 0.002) || TestNonStiffEquation<ExplicitSDC>())
    {
        exit(1);
    }
    exit(0);
}
