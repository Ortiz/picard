// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "Utilities/Logger.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestLogger(int ac, char **av)
{
    auto logger = Logger::instance();

    auto log = logger->get("testing");
    logger->setLevel(spdlog::level::off, "console");
    logger->setLevel(spdlog::level::off, "file");

    auto anotherLogger = Logger::instance();
    if (logger != anotherLogger)
    {
        exit(1);
    }

    if (log != anotherLogger->get("testing"))
    {
        exit(1);
    }
    exit(0);
}
