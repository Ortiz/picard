// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

// STL includes
#include <iostream>

// Picard includes
#include "NonLinearSolvers/NewtonMethod.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestNewtonMethod(int, char **)
{
    using VectorType = NewtonMethod::VectorType;

    auto f = [](const VectorType &x)
    {
        auto y = x;

        y[0] = x[2] * x[0] + x[0] * x[2] - x[0] * x[0] - 1.0;
        y[1] = x[1] * x[1] + x[1] * x[1] - x[1] * x[1] - 1.0;
        y[2] = x[0] * x[2] + x[2] * x[0] - x[2] * x[2] - 1.5;

        return y;
    };

    auto solver = NewtonMethod::New(f);
    {
        auto nsolver0 = NewtonMethod(*solver);             // Copy
        auto nsolver1 = NewtonMethod(std::move(nsolver0)); // Move

        nsolver0 = nsolver1;            // Assign
        nsolver1 = std::move(nsolver0); // Assign move
    }

    VectorType x0(3);

    x0.setRandom();

    solver->setLogLevel(spdlog::level::off, "console");
    solver->init(x0);
    solver->solve();

    if (f(solver->get()).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(0);
    }

    exit(1);
}
