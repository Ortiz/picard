// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

#include <Eigen/Jacobi>

#include "LinearSolvers/ArnoldiIteration.h"
#include "LinearSolvers/GeneralizedMinimalResidual.h"

using MatrixType = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;
using VectorType = Eigen::Matrix<double, Eigen::Dynamic, 1>;

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestArnoldiIteration(int ac, char **av)
{
    // Setup supporting matrices and vectors
    MatrixType v = MatrixType::Zero(3, 4);
    MatrixType h = MatrixType::Zero(4, 3);
    VectorType g = VectorType::Zero(4);

    // Arnoli instantiation.
    // Recall that Arnoldi iteration will orthogonalize the columns of the matrix v.
    // It will also try to re-orthogonalize if the norm of the current column vector is smaller than 0.0001.
    ArnoldiIteration arnoldi(0.0001);

    // JacobiRotations are used to create the Givens rotation matrix that will cancel the 2nd entry in the
    // columns of h.
    std::vector<Eigen::JacobiRotation<double>> R(3);

    // System matrix
    MatrixType A(3, 3);
    A << 1, 0, -1, 0, .01, 0, 1, 0, .000001; // cppcheck-suppress constStatement

    // Rhs
    VectorType b(3);
    b << 1, 1, 0; // cppcheck-suppress constStatement
    // Build the v columns as v = [r, Ar, A^2r, ...] spaning the Krylov space
    // Set initial iterate to zero then the residual is v(0) = b - A*zero == b
    v.col(0) = b;

    // Save residual norms in g
    g(0) = v.col(0).norm();
    v.col(0) /= g(0);

    // v(1) = Ar
    v.col(1) = A * v.col(0);

    // Orthogonalize column 0 and column 1
    arnoldi(v, h, 0);

    // Check that the columns are orthogonal
    if (v.col(0).dot(v.col(1)) > 1e-14)
    {
        exit(1);
    }

    // Compare vector v(1) with truth vector
    VectorType v_truth(3);
    v_truth << 0.4055129968046616, -0.4055129968046614, // cppcheck-suppress constStatement
      0.8192181753629526;
    if (!(v.col(1) - v_truth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    // Use a Givens rotation matrix to cancel the value of h(1,0), this value must be set to zero in order for
    // the resulting matrix h to be square and upper triangular. The main goal is to solve the least-squares
    // system h*y = g. This in effect will minimize the residual on the Krilov space {r, Ar, A^2r, A^3r, ...}
    R[0].makeGivens(h(0, 0), h(1, 0));
    h.col(0).applyOnTheLeft(0, 1, R[0].adjoint());

    // Make sure h(1,0) ~= 0
    if (h(1, 0) > 1e-14)
        exit(1);

    // Apply rotation to the rhs g.
    g.applyOnTheLeft(0, 1, R[0].adjoint());

    // Repeat process for column 3 and 4
    //--------------------------------------------------------------------------------------------
    v.col(2) = A * v.col(1);
    arnoldi(v, h, 1);

    v_truth << -0.5792747270704142, 0.5792747270704139, // cppcheck-suppress constStatement
      0.5734819797997097;
    if (!(v.col(2) - v_truth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    if (v.col(1).dot(v.col(2)) > 1e-14)
    {
        exit(1);
    }

    h.col(1).applyOnTheLeft(0, 1, R[0].adjoint());

    R[1].makeGivens(h(1, 1), h(2, 1));
    h.col(1).applyOnTheLeft(1, 2, R[1].adjoint());

    // Make sure h(2,1) ~= 0
    if (h(2, 1) > 1e-14)
        exit(1);

    g.applyOnTheLeft(1, 2, R[1].adjoint());

    v.col(3) = A * v.col(2);
    arnoldi(v, h, 2);

    v_truth << -0.05607721540920443, 0.6168493695012488, // cppcheck-suppress constStatement
      -0.785081015728862;
    if (!(v.col(3) - v_truth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    h.col(2).applyOnTheLeft(0, 1, R[0].adjoint());

    h.col(2).applyOnTheLeft(1, 2, R[1].adjoint());
    R[2].makeGivens(h(2, 2), h(3, 2));
    h.col(2).applyOnTheLeft(2, 3, R[2].adjoint());

    // Make sure h(3,2) ~= 0
    if (h(3, 2) > 1e-14)
    {
        exit(1);
    }

    g.applyOnTheLeft(2, 3, R[2].adjoint());

    //----------------------------------------------------------------------------------------------

    MatrixType H_thruth(4, 3);
    H_thruth << 1.000024999687508, -0.005820696579371287, -1.224659189648804, -5.551115123125783e-17,
      0.5792892138834793, 0.4054035206894527, 0, -5.551115123125783e-17, 0.01726212014563064, 0, 0, 0;

    if (!(h - H_thruth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    VectorType g_thruth(4);
    g_thruth << 0.7141599952217017, -0.7139842931172214, 0.9900514786650445, -3.15164773727096e-30;
    if (!(g - g_thruth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    // Finally, solve the least-squares system and compare solution
    Eigen::Ref<VectorType> y = g.head(3);
    h.topRightCorner(3, 3).template triangularView<Eigen::Upper>().solveInPlace(y);

    VectorType y_thruth(3);
    y_thruth << 70.71067882576141, -41.37051663109946, 57.35399072144941;

    if (!(y - y_thruth).isMuchSmallerThan(1.0, 1e-15))
    {
        exit(1);
    }

    exit(0);
}
