// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

// Picard includes
#include "ODESolvers/Euler/BackwardEuler.h"
#include "Utilities.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestBackwardEuler(int ac, char **av)
{
    using VectorType = BackwardEuler::VectorType;

    // Cover construction and assignament
    {
        auto stepper = BackwardEuler::New(0,
                                          [](const double t, const VectorType &x)
                                          {
                                              return x;
                                          });

        auto stepper0 = BackwardEuler(*stepper);
        auto stepper1 = BackwardEuler(std::move(stepper0));

        stepper0 = stepper1;
        stepper1 = std::move(stepper0);
    }

    if (TestStiffEquation<BackwardEuler>() || TestNonStiffEquation<BackwardEuler>())
    {
        exit(1);
    }
    exit(0);
}
