// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include "NonLinearSolvers/ArmijoLineSearch.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestArmijoLineSearch(int ac, char **av)
{
    using VectorType = ArmijoLineSearch::VectorType;

    VectorType x(3);
    VectorType dx(3);
    x.setRandom();
    dx.setRandom();
    auto f = [](const VectorType &x)
    {
        auto y = x;

        y[0] = x[2] * x[0] + x[0] * x[2] - x[0] * x[0] - 1.0;
        y[1] = x[1] * x[1] + x[1] * x[1] - x[1] * x[1] - 1.0;
        y[2] = x[0] * x[2] + x[2] * x[0] - x[2] * x[2] - 1.5;

        return y;
    };
    ArmijoLineSearch armijo(f);

    std::array<double, 2> functionNorms = {f(x).norm()};

    auto lambda = armijo(x, dx, functionNorms);

    if (armijo.getConvergenceStatus() != ArmijoLineSearch::Status::Converged)
    {
        exit(1);
    }
    exit(0);
}
