// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

#include "ODESolvers/SDC/SemiImplicitSDC.h"

// Solve the diffusion Brusselator equations
int solveDiffusion()
{
    using VectorType = SemiImplicitSDC::VectorType;

    double A     = 1.0;
    double B     = 1.0;
    double alpha = 1.0;
    size_t size  = 100;
    double x0    = 0.0;
    double xN    = 1.0;
    double dx    = (xN - x0) / size / 2;

    auto f = [A, B, alpha, dx](const VectorType &x)
    {
        auto size = x.size();
        VectorType y(size);
        size_t i = 0, j = 1;
        // clang-format off
        y(i) = A + x(i) * x(i) * x(j) - (B + 1.0) * x(i) + alpha * 1.0 / (dx * dx) * (1.0 - 2.0 * x(i) + x(i + 2));
        y(j) = B * x(i) - x(i) * x(i) * x(j)             + alpha * 1.0 / (dx * dx) * (3.0 - 2.0 * x(j) + x(j + 2));

        for (i = 2, j = 3; j < size - 2; i += 2, j += 2)
        {
            y(i) = A + x(i) * x(i) * x(j) - (B + 1.0) * x(i) + alpha * 1.0 / (dx * dx) * (x(i - 2) - 2.0 * x(i) + x(i + 2));
            y(j) = B * x(i) - x(i) * x(i) * x(j)             + alpha * 1.0 / (dx * dx) * (x(j - 2) - 2.0 * x(j) + x(j + 2));
        }

        i = size - 2, j = size - 1;
        y(i) = A + x(i) * x(i) * x(j) - (B + 1.0) * x(i) + alpha * 1.0 / (dx * dx) * (x(i - 2) - 2.0 * x(i) + 1.0);
        y(j) = B * x(i) - x(i) * x(i) * x(j)             + alpha * 1.0 / (dx * dx) * (x(j - 2) - 2.0 * x(j) + 3.0);
        // clang-format on
    };
    return 0;
}

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestSemiImplicitSDC(int, char **)
{
    using VectorType = SemiImplicitSDC::VectorType;

    double dt    = 0.1;
    auto stepper = SemiImplicitSDC::New(
      dt,
      [](const double t, const VectorType &x)
      {
          return x;
      },
      [](const double t, const VectorType &x)
      {
          return -x.array() * x.array();
      });
    {
        auto stepper0 = SemiImplicitSDC(*stepper);
        auto stepper1 = SemiImplicitSDC(std::move(stepper0));

        stepper0 = stepper1;
        stepper1 = std::move(stepper0);
    }

    auto solution = [](const double t)
    {
        VectorType s(1);
        s(0) = 1.0 / (-0.5 * std::exp(-t) + 1.0);
        return s;
    };

    VectorType x0(1);
    x0(0) = 2.0;
    stepper->init(0.0, x0);
    for (size_t s = 0; s < 120; ++s)
    {
        stepper->step();
    }

    if (!(stepper->get() - solution(119 * dt)).isMuchSmallerThan(1.0, dt))
    {
        exit(1);
    }
    exit(0);
}
