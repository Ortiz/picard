// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

// STL includes
#include <chrono>
#include <iostream>
#include <thread>

// Picard includes
#include "Utilities/Timer.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestTimer(int ac, char **av)
{
    using namespace std::chrono_literals;
    {
        auto t = Timer("Test2 - how long this takes");
        std::this_thread::sleep_for(1000ms);
        // Account for the internal multiplication and division.
        if (1.0 - 1000.0 / t.get() > .1)
        {
            exit(1);
        }
    }
    {
        auto t = Timer("Test1 - how long this takes");
        std::this_thread::sleep_for(1000ms);
        // Account for the division and multiplication - This is faster.
        if (1.0 - 1000.0 / (t.end() * .001) > .1)
        {
            exit(1);
        }
    }
    exit(0);
}
