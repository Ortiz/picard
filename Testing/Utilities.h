// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <fstream>

// Externals
#include <spdlog/spdlog.h>

template <typename TimeStepperType>
int TestStiffEquation(const double dt = 0.1, const double errorTol = 0.1)
{
    using VectorType = typename TimeStepperType::VectorType;

    auto stepper = TimeStepperType::New(dt,
                                        [](const double t, const VectorType &x)
                                        {
                                            return -1000 * x.array() + 3000 - 2000 * std::exp(-t);
                                        });

    stepper->setLogLevel(spdlog::level::err, "file");

    VectorType x0(1);
    x0(0) = 0.0;
    stepper->init(0.0, x0);

    auto solution = [x0](double t)
    {
        VectorType s(1);
        s(0) = 3.0 - 0.998 * std::exp(-1000 * t) - 2.002 * std::exp(-t);
        return s;
    };

    VectorType approxSol(1);
    VectorType sol(1);
    for (size_t s = 1; s < 120; ++s)
    {
        stepper->step();
    }
    approxSol(0) = stepper->get()(0);
    sol(0)       = solution(119 * dt)(0);

    if (!(approxSol - sol).isMuchSmallerThan(1.0, errorTol))
    {
        return 1;
    }

    return 0;
}

template <typename TimeStepperType>
int TestNonStiffEquation(const double dt = 0.1, const double errorTol = 0.1)
{
    using VectorType = typename TimeStepperType::VectorType;

    auto stepper = TimeStepperType::New(dt,
                                        [](const double t, const VectorType &x)
                                        {
                                            return -std::exp(-t) * x.array() * x.array();
                                        });

    stepper->setLogLevel(spdlog::level::err, "file");

    VectorType x0(1);
    x0(0) = 2.0;
    stepper->init(0.0, x0);

    auto solution = [x0](double t)
    {
        VectorType s(1);
        s(0) = -std::exp(t) / (-1.5 * std::exp(t) + 1);
        return s;
    };

    VectorType approxSol(1);
    VectorType sol(1);
    for (size_t s = 1; s < 120; ++s)
    {
        stepper->step();
    }
    approxSol(0) = stepper->get()(0);
    sol(0)       = solution(119 * dt)(0);

    if (!(approxSol - sol).isMuchSmallerThan(1.0, errorTol))
    {
        return 1;
    }

    return 0;
}

template <typename VectorType>
void plotOctave(const VectorType &x, const VectorType &y)
{
    std::ofstream output("./out.m");
    auto size = x.size();
    output << "x = [";
    for (int i = 0; i < size; ++i)
        output << x[i] << " ";
    output << "];\n";
    output << "y = [";
    for (int i = 0; i < size; ++i)
        output << y[i] << " ";
    output << "];\nplot(x,y);\n";
    output.close();
}
