// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

#include "ODESolvers/Euler/ForwardEuler.h"
#include "Utilities.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestForwardEuler(int, char **)
{
    using VectorType = ForwardEuler::VectorType;

    // Cover construction and assignament
    {
        auto stepper = ForwardEuler::New(0,
                                         [](const double t, const VectorType &x)
                                         {
                                             return x;
                                         });

        auto stepper0 = ForwardEuler(*stepper);
        auto stepper1 = ForwardEuler(std::move(stepper0));

        stepper0 = stepper1;
        stepper1 = std::move(stepper0);
    }

    if (TestStiffEquation<ForwardEuler>(.001) || TestNonStiffEquation<ForwardEuler>())
    {
        exit(1);
    }
    exit(0);
}
