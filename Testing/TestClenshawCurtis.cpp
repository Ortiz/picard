// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#include <iostream>

#include "ODESolvers/SDC/Quadratures/ClenshawCurtis.h"

// CMake testing routine. This will be called by the CMake-generated test driver.
int TestClenshawCurtis(int, char **)
{
    ClenshawCurtis quad;
    using QuadSize = ClenshawCurtis::QuadSize;

    auto m2 = quad.matrix<QuadSize::Two>();
    std::cout << m2 << std::endl;

    // std::cout << "Class name: " << stepper->getName() << std::endl;

    // VectorType x0(1);
    // x0(0) = 0.1;
    // stepper->init(0.0, x0);

    // auto solution = [x0](double t)
    // {
    //     VectorType s(1);
    //     s(0) = 0.5 * t*t + x0(0);
    //     return s;
    // };

    // for(size_t s = 0; s < 120; ++s)
    // {
    //     stepper->step();
    // }

    // if((stepper->get() - solution(120*dt)).norm() > 0.01)
    // {
    //     exit(1);
    // }

    exit(0);
}
