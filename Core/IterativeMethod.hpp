// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <unordered_map>
#include <vector>

// Picard includes
#include "Core/Object.hpp"

template <typename Derived>
class IterativeMethod : public Object<Derived>
{
public:
    using Self = IterativeMethod<Derived>;

    enum class Status
    {
        Diverged,
        Converged,
        Unknown
    };

public:
    IterativeMethod()          = default;
    virtual ~IterativeMethod() = default;

    void computeErrors() { static_cast<Derived *>(this)->computeErrors(); }

    void solutions() { }

protected:
    bool collectStats = false;
    std::unordered_map<std::string, std::vector<double>> statsQuantities;
    std::unordered_map<std::string, Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>> statsMatrices;
    std::unordered_map<std::string, Eigen::Matrix<double, Eigen::Dynamic, 1>> statsVectors;
};
