// Copyright (c) 2021-present Rick Ortiz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)

#pragma once

// STL includes
#include <cxxabi.h>
#include <memory>
#include <string>
#include <unordered_map>

// Picard includes
#include "Utilities/Logger.h"

template <typename Derived>
class Object
{
public:
    using Base = Object<Derived>;
    using Ptr  = std::shared_ptr<Derived>;

    template <typename... Args>
    static Ptr New(Args &&...args)
    {
        return std::make_shared<Derived>(args...);
    }

public:
    Object()
    {
        // Get derived class name
        char *demangled = abi::__cxa_demangle(typeid(*static_cast<Derived *>(this)).name(), 0, 0, 0);
        Base::name      = demangled;
        delete demangled;

        // Increase Derived instances count
        Base::instances[Base::name]++;
    }

    virtual ~Object()
    {
        // Decrease Derived instances count
        Base::instances[Base::name]--;
    }

    inline static const std::string &getName() { return Base::name; }

    size_t getInstances() const { return Base::instances[this->name]; }

    inline static Logger::AsyncPtr getLogger() { return Logger::instance()->get(Base::name); }

    void setLogLevel(const spdlog::level::level_enum level, const std::string &sinkType = "console")
    {
        Logger::instance()->setLevel(level, sinkType);
    }

private:
    static std::string name;
    static std::unordered_map<std::string, size_t> instances;
};

template <typename Derived>
std::unordered_map<std::string, size_t> Object<Derived>::instances;

template <typename Derived>
std::string Object<Derived>::name;
