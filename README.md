# Picard

## Intro

Picard is a C++ library that implements several time-stepping schemes for solving Initial Value Problems (IVP) that arise in physics-based simulations, often requiring solving IVPs that are stiff in nature, that is, the solution to these problems have components that have relative large variations when compared to other components.  Picard aims at implementing schemes that address these stiff problems using Spectral Deferred Correction (SDC) methods. Picard's SDC methods can be seen as a generic frame work for solving IVPs and depending on your specific needs it can be instructed to use Explicit or Implicit methods or even a combination of these.

Solving the Initial Value Problem (IVP)

```math
\begin{align*}
\frac{dx}{dt}&=F(t, x(t)) \\
x(t_0)&=x_0
\end{align*}
```

is equivalent to solving the Picard integral:

```math
x(t) = x(t_0) + \int_{t_0}^t F(\tau, x(\tau))d\tau
```

Now suppose we have an approximation to the solution (presumably obtained by a low order inexpensive solver) $`\color{red}\tilde{x}(\tau)`$, then we can measure the quality of this approximation by computing the residual:

```math
R(t,{\color{red}\tilde{x}(t)})=x_{0}+\int_{t_0}^{t}F(\tau,{\color{red}\tilde{x}(\tau)})d\tau-{\color{red}\tilde{x}(t)}
```

Thus, the error $`\delta(t)=x(t)-\color{red}\tilde{x}(t)`$ is given by:

```math
\delta(t)=\int_{t_0}^{t}F(\tau,{\color{red}\tilde{x}(\tau)}+\delta(\tau))-F(\tau,{\color{red}\tilde{x}(\tau)})d\tau+R(t,{\color{red}\tilde{x}(t)})
```

The main strategy of the SDC method is to compute a provisional solution $`\color{red}\tilde{x}(t)`$ on an interval $`[t_i,t_{i+1}]`$, then use the correction equation above to iteratively improve the provisional solution.

## Algorithms

Currently, Picard provides APIs to the following algorithms:

1. Forward and Backward Euler methods.
2. Explicit and Semi-Implicit implementation of the Spectral Correction Method. The semi-implicit version takes two function as right-hand side.
3. Generalized Residual Method. Matrix-free implementation for solving linear systems of equations, $`Ax=b`$.
4. Newton Method with backtracking. Inexact Newton implementation, matrix-free Jacobians are approximated by finite differences. Used as a non linear solver for the Backward Euler method.

## Dependencies

1. [Eigen](https://gitlab.com/libeigen/eigen)
2. [spdlog](https://github.com/gabime/spdlog)
